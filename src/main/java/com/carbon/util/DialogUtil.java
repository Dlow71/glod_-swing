package com.carbon.util;

import javax.swing.*;

public class DialogUtil {
    /**
     * 错误提示
     * @param message
     */
    public static void errorDialog(String message){
        JOptionPane.showMessageDialog(null,message,"错误",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * 警告提示
     * @param message
     */
    public static void msgDialog(String message){
        JOptionPane.showMessageDialog(null,message,"提示",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * 确认对话框
     * @param message
     * @return
     */
    public static int confirm(String message){
        return JOptionPane.showConfirmDialog(null,message,"提示",JOptionPane.YES_NO_OPTION);
    }
}
