package com.carbon.util;

import java.util.Map;
import java.util.stream.Stream;

/**
 * 键值对操作的工具类
 */
public class MapUtil {

    /**
     * 根据map的value获取map的key
     * 注意：若value相同的值可能有很多个，要返回多个key值。
     *      就要把找到的key标记，下次不再用，一起返回。
     *
     * @param map   需要获取key的map
     * @param value 指定value
     * @return 返回key
     */
    public static String getKey(Map<String, String> map, String value) {
        if(StringUtils.isNull(value)) return null;
        if(StringUtils.isEmpty(value)) return "";
        // 初始化key
        String key = "";
        // 临时存放key，
//        List<String> containedKey = new ArrayList<>();
        /**
         * 遍历map
         */
        for (Map.Entry<String, String> entry : map.entrySet()) {
            /**
             * 如果value和key对应的value相同 并且 key不在list中
             */
//            if (value.equals(entry.getValue()) && (!containedKey.contains(entry.getKey()))) {
            if (value.equals(entry.getValue())) {
                key = entry.getKey();
//                containedKey.add(entry.getKey());
                break;
            }
        }
        return key;
    }

}
