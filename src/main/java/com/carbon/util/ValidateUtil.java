package com.carbon.util;

import java.util.Collection;
import java.util.Map;

/**
 * 正则校验
 */
public class ValidateUtil {

    /**
     * 电话校验
     * @param s_dz
     * @return
     */
    public static boolean  telValidate(String s_dz){
        if(s_dz.matches("^((0\\d{2,3})?[-]?\\d{7,8})$")){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 手机校验
     * @param s_ds
     * @return
     */
    public static boolean phoneValidate(String s_ds){
        if(s_ds.matches("^[1][3,4,5,6,7,8,9][0-9]{9}$")){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 数字小数点校验
     * @param s_ds
     * @return
     */
    public static boolean numberValidate(String s_ds){
        if(s_ds.matches("([1-9]\\d*\\.?\\d*)|(0\\.\\d*[1-9])")){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 日期校验
     * @param s_ds
     * @return
     */
    public static boolean dateValidate(String s_ds){
        if(s_ds.matches("^\\d{4}-\\d{1,2}-\\d{1,2}")||
                s_ds.matches("^(0?[1-9]|1[0-2])$")||
                s_ds.matches("^((0?[1-9])|((1|2)[0-9])|30|31)$")) {
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * 判断Collection是否为null或size为0
     * @param obj 接收任意一个集合
     * @return 布尔值
     */
    public static boolean isEmpty(Collection<?> obj){
        return obj==null || obj.isEmpty();
    }

    /**
     * String是否为null或""
     *
     * @param obj String
     * @return 是否为空
     */
    public static boolean isEmpty(String obj) {
        return (obj == null || "".equals(obj));
    }

    /**
     * Array是否为null或者size为0
     *
     * @param obj Array
     * @return 是否为空
     */
    public static boolean isEmpty(Object[] obj) {
        return obj == null || obj.length == 0;
    }

    /**
     * Map是否为null或size为0
     *
     * @param obj Map
     * @return 是否为空
     */
    public static boolean isEmpty(Map<?, ?> obj) {
        return obj == null || obj.isEmpty();
    }


}
