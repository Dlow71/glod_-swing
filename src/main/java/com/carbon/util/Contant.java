package com.carbon.util;

public class Contant {
    //审批通过
    public static final String AUDIT_POST="1";
    //审批驳回
    public static final String AUDIT_REJECT="0";
    //未提交
    public static final String APPLY_NO_SUBMIT="0";
    //已提交
    public static final String APPLY_SUBMIT="1";
    //已审核
    public static final String APPLY_AUDIT="2";

    //未入库
    public static final String IN_NO_WAREHOUSE="0";
    //未出库
    public static final String OUT_NO_WAREHOUSE="0";
    //空闲的设备
    public static final String NO_LINE="0";

    //没有领料状态
    public static final String NO_RECEIVE_MATERIAL="0";

    //业务类型(3-领料  4-销售
    public static final String EVENT_TYPE_MATERIAL="3";
    public static final String EVENT_TYPE_SALE="4";

}
