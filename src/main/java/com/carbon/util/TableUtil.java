package com.carbon.util;

import javax.swing.*;
import javax.swing.table.DefaultTableColumnModel;

/**
 * 表格操作工具类
 */
public class TableUtil {

    public static void hiddenColumn(JTable table, int columnIndex){
        DefaultTableColumnModel columnModel= (DefaultTableColumnModel) table.getColumnModel();
        columnModel.getColumn(columnIndex).setMaxWidth(0);
        columnModel.getColumn(columnIndex).setMinWidth(0);
        columnModel.getColumn(columnIndex).setPreferredWidth(0);
    }
}
