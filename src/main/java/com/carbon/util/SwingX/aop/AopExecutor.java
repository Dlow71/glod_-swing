package com.carbon.util.SwingX.aop;

import com.carbon.util.ValidateUtil;
import com.carbon.util.SwingX.aop.aspect.AspectInfo;
import com.carbon.util.SwingX.aop.aspect.AspectInfoPlus;
import com.carbon.util.SwingX.aop.aspect.AspectInfoTemplate;
import com.carbon.util.SwingX.ioc.BeanContainer;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 统一织入执行器
 * 之所以叫统一，就是因为有两套AOP方案
 * 一套是识别注解，一套是识别表达式，都需要织入
 * 而且两个是可以一起使用的，比如说第一套就注解指定了切入@Service,且order值为1
 * 然后第二套用表达式刚好也指定了该类，且order值为0，那么执行顺序就是先第二套再第一套
 * 总之多尝试几次就知道了
 * 人都写麻了...
 */
public class AopExecutor {
    // 切面类容器  之所以是用Annotation为key，是因为只有注入容器的类才能被切面
    private Map<Class<? extends Annotation>, List<AspectInfoTemplate>> categorizedMap = new HashMap<>();

    private static BeanContainer beanContainer;
    static{
        beanContainer = BeanContainer.getInstance();
    }

    public void putCategoryMap(Map<Class<? extends Annotation>, List<AspectInfo>> map){
        for (Class<? extends Annotation> annotation : map.keySet()) {
            if(categorizedMap.containsKey(annotation)){
                List<AspectInfoTemplate> aspectInfoTemplates = categorizedMap.get(annotation);
                aspectInfoTemplates.addAll(map.get(annotation));
            }else{
                // 骗一下编译器 编译器这老6我真的是服了，直接add还add不进去
                List<AspectInfoTemplate> list = new ArrayList<>();
                list.addAll(map.get(annotation));
                categorizedMap.put(annotation,list);
            }
        }
    }


    public void putCategoryMapPlus(Map<Class<? extends Annotation>, List<AspectInfoPlus>> map){
        for (Class<? extends Annotation> annotation : map.keySet()) {
            if(categorizedMap.containsKey(annotation)){
                List<AspectInfoTemplate> aspectInfoTemplates = categorizedMap.get(annotation);
                aspectInfoTemplates.addAll(map.get(annotation));
            }else{
                // 骗一下编译器
                List<AspectInfoTemplate> list = new ArrayList<>();
                list.addAll(map.get(annotation));
                categorizedMap.put(annotation,list);
            }
        }
    }


    public void executeWeave(){
        // 多线程执行，加快初始化速度
        AopCallable aopCallable = new AopCallable();
        AopPlusCallable aopPlusCallable = new AopPlusCallable();
        FutureTask<Map<Class<? extends Annotation>, List<AspectInfo>>> aopTask = new FutureTask<>(aopCallable);
        FutureTask<Map<Class<? extends Annotation>, List<AspectInfoPlus>>> aopPlusTask = new FutureTask<>(aopPlusCallable);
        Thread aopThread = new Thread(aopTask);
        Thread aopPlusThread = new Thread(aopPlusTask);
        // 多线程同时获取分类好的所有切面类
        aopThread.start();
        aopPlusThread.start();
        try {
            // 获取任务执行完的结果，即分类好的切面类
            Map<Class<? extends Annotation>, List<AspectInfo>> aopMap = aopTask.get();
            putCategoryMap(aopMap);
            Map<Class<? extends Annotation>, List<AspectInfoPlus>> aopPlusMap = aopPlusTask.get();
            putCategoryMapPlus(aopPlusMap);

            // 切面类既然已经分类好了，就可以开始织入了
            for (Class<? extends Annotation> category : categorizedMap.keySet()) {
                weaveByCategory(category,categorizedMap.get(category));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * 按照不同的织入目标分别取按序织入Aspect的逻辑
     * @param category 织入对象
     * @param aspectInfos Aspect集合
     */
    public void weaveByCategory(Class<? extends Annotation> category, List<AspectInfoTemplate> aspectInfos) {
        // 获取被代理类的集合
        Set<Class<?>> classSet = beanContainer.getClassesByAnnotation(category);
        if(ValidateUtil.isEmpty(classSet)) return;
        for (Class<?> targetClass : classSet) {
            // 切面集合执行器 因为一个注解会被多个Aspect标识，将织入逻辑织入指定类
            AspectListExecutor aspectListExecutor = new AspectListExecutor(targetClass, aspectInfos);
            // 创建动态代理对象 相当于一次性织入了好几个切面
            Object proxy = ProxyCreator.createProxy(targetClass, aspectListExecutor);
            // 使用代理对象取代未被代理前的类实例
            beanContainer.addBean(targetClass,proxy);
        }
    }
}

class AopCallable implements Callable<Map<Class<? extends Annotation>, List<AspectInfo>>>{

    @Override
    public Map<Class<? extends Annotation>, List<AspectInfo>> call() throws Exception {
        return new AspectWeaver().doAop();
    }
}

class AopPlusCallable implements Callable<Map<Class<? extends Annotation>,List<AspectInfoPlus>>>{

    @Override
    public Map<Class<? extends Annotation>, List<AspectInfoPlus>> call() throws Exception {
        return new AspectWeaverPlus().doAopPlus();
    }
}
