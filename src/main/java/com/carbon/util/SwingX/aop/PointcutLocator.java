package com.carbon.util.SwingX.aop;

import org.aspectj.weaver.tools.PointcutExpression;
import org.aspectj.weaver.tools.PointcutParser;
import org.aspectj.weaver.tools.ShadowMatch;

import java.lang.reflect.Method;

//解析Aspect表达式，并定位被织入的目标
public class PointcutLocator {
    /**
     * pointcut解析器，直接给他赋值上AspectJ的所有表达式，以便支持众多表达式的解析
     */
    private PointcutParser pointcutParser = PointcutParser.getPointcutParserSupportingSpecifiedPrimitivesAndUsingContextClassloaderForResolution(
            PointcutParser.getAllSupportedPointcutPrimitives()
    );

    /**
     * 表达式解析器，用来判断某个类是否被匹配
     */
    private PointcutExpression pointcutExpression;

    public PointcutLocator(String expression){
        pointcutExpression = pointcutParser.parsePointcutExpression(expression);
    }

    /**
     * 针对类级别的判断
     * 判断传入的class对象是否是Aspect的目标代理类，即匹配PointCUT表达式（初步筛选）
     * @param targetObject 目标类
     * @return 是否匹配
     */
    public boolean roughMatches(Class<?> targetObject){
        // couldMatchJoinPointsInType这个方法比较坑，只能校验within
        // 不能校验(execution，call，get，set)，面对无法校验的表达式，直接返回true
        return pointcutExpression.couldMatchJoinPointsInType(targetObject);
    }

    /**
     * 判断传入的method对象是否是Aspect的目标代理方法，即匹配Pointcut表达式(精筛)
     * @param method 目标方法
     * @return 目标方法是否匹配
     */
    public boolean accurateMatches(Method method){
        // 对传入的method实例进行精准筛选
        ShadowMatch shadowMatch = pointcutExpression.matchesMethodExecution(method);
        // 这个方法就是与表达式完全匹配才会返回true，否则返回false
        if(shadowMatch.alwaysMatches()){
            return true;
        }else{
            return false;
        }
    }
}
