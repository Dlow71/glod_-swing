package com.carbon.util.SwingX.aop.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {
    // value值只能是注解类型，用法是，如果指定value为@Service，那么所有Service都会加上横切逻辑
    Class<? extends Annotation> value();
}
