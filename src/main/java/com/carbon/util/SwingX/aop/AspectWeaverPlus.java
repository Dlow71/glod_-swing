package com.carbon.util.SwingX.aop;

import com.carbon.common.aspect.ServiceTimeCalcAspect;
import com.carbon.util.ValidateUtil;
import com.carbon.util.SwingX.aop.annotation.Aspect;
import com.carbon.util.SwingX.aop.annotation.AspectPlus;
import com.carbon.util.SwingX.aop.annotation.Order;
import com.carbon.util.SwingX.aop.aspect.AspectInfo;
import com.carbon.util.SwingX.aop.aspect.AspectInfoPlus;
import com.carbon.util.SwingX.aop.aspect.AspectInfoTemplate;
import com.carbon.util.SwingX.aop.aspect.DefaultAspect;
import com.carbon.util.SwingX.ioc.BeanContainer;


import java.lang.annotation.Annotation;
import java.util.*;

public class AspectWeaverPlus {
    private BeanContainer beanContainer;
    private Map<Class<? extends Annotation>,List<AspectInfoPlus>> categoryMap = new HashMap<>();
    private List<Class<? extends Annotation>> beanAnnotation = BeanContainer.getBeanAnnotation();

    public AspectWeaverPlus(){
        // 因为beanContain本身就是单例的，所以可以直接这样获取
        beanContainer = BeanContainer.getInstance();
    }

    public Map<Class<? extends Annotation>,List<AspectInfoPlus>> doAopPlus(){
        // 1、获取所有被@AspectPlus标记的切面类
        Set<Class<?>> aspectSet = beanContainer.getClassesByAnnotation(AspectPlus.class);
        if(ValidateUtil.isEmpty(aspectSet)){
            return new HashMap<>();
        }
        // 2、直接拼装aspectInfoList
        List<AspectInfoPlus> aspectInfoList = packAspectInfoList(aspectSet);

        // 3、获取并遍历容器里的类
        Set<Class<?>> classSet = beanContainer.getClasses();
        for (Class<?> targetClass : classSet) {
            // 3.1、排除自身 和 一代AOP 同时该class必须被注解标记过，否则无法织入Aspect
            if(targetClass.isAnnotationPresent(AspectPlus.class) || targetClass.isAnnotationPresent(Aspect.class) || !verifyBean(targetClass)){
                continue;
            }
            // 4、粗略筛选符合条件的aspect
            List<AspectInfoPlus> roughMatchedAspectList = collectRoughMatchedAspectListForSpecificClass(aspectInfoList,targetClass);
            if(ValidateUtil.isEmpty(roughMatchedAspectList)) continue;
            // 5、尝试进行Aspect的织入(已经改成统一用AopExecutor去织入了)
//            wrapIfNecessary(roughMatchedAspectList,targetClass);

            // 5、根据注解分类
            // 只有是容器里面的对象才能被织入，因此先获取该类上的所有注解，看是否使用的是我们的注解
            Annotation[] annotations = targetClass.getAnnotations();
            flag:for (Annotation annotation : annotations) {
                // 注意这里不能使用getClass!! 必须使用annotationType
                Class<? extends Annotation> annotationClass = annotation.annotationType();
                for (Class<? extends Annotation> requireAnnotation : beanAnnotation) {
                    // 如果发现使用了我们的注解，那就可以被织入
                    if(annotationClass==requireAnnotation){
                        putMap(annotationClass,roughMatchedAspectList);
                        break flag;
                    }
                }
            }
        }
        // 返回分类好的AspectInfoPlus集合
        return categoryMap;
    }

    public static void main(String[] args) {
        Annotation[] annotations = ServiceTimeCalcAspect.class.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }
//        List<? extends AspectInfoTemplate> list = new ArrayList<>();
        // 泛型的上限<? extends T>用在频繁获取数据的场景
        // 泛型的下限<? super T>用在频繁添加数据的场景
        // https://blog.csdn.net/weixin_40251892/article/details/109063161
        List<? super String> list = new ArrayList<>();
        list.add("213");

//        Map<Class<? extends Annotation>, List<? super AspectInfo>> one = new AspectWeaver().doAop();
        Map<Class<? extends Annotation>, List<AspectInfoPlus>> two = new AspectWeaverPlus().doAopPlus();
        Map<Class<? extends Annotation>, List<? extends AspectInfoTemplate>> three = new HashMap<>();
        List<AspectInfoPlus> aspectInfoTemplates = (List<AspectInfoPlus>) two.get("");
        List<? super AspectInfoPlus> aspectInfoTemplates1 = (List<? super AspectInfoPlus>) three.get("");
        List<? extends AspectInfoTemplate> aspectInfoTemplates2 = two.get("");
        //        aspectInfoTemplates.add(aspectInfoTemplates1.get(0))
//        two.putAll((Map<? extends Class<? extends Annotation>, ? extends List<? extends AspectInfoTemplate>>) one);
        System.out.println(two);
//        Map<>
    }


    public void putMap(Class<? extends Annotation> clazz,List<AspectInfoPlus> infoPlusList){
        if(categoryMap.containsKey(clazz)){
            List<? super AspectInfoPlus> aspectInfoPlusList = (List<? super AspectInfoPlus>) categoryMap.get(clazz);
            aspectInfoPlusList.addAll(infoPlusList);
        }else{
            categoryMap.put(clazz,infoPlusList);
        }
    }

    /**
     * 检查该class类是否被注解标记，否则不是容器里面的对象无法织入
     * @param clazz 目标class类
     * @return 检查标记
     */
    public boolean verifyBean(Class<?> clazz){
        for (Class<? extends Annotation> annotation : beanAnnotation) {
            if(clazz.isAnnotationPresent(annotation)){
                return true;
            }
        }
        return false;
    }

    private void wrapIfNecessary(List<AspectInfoPlus> roughMatchedAspectList, Class<?> targetClass) {
        if(roughMatchedAspectList.isEmpty()) return;
        AspectListExecutor aspectListExecutor = new AspectListExecutor(targetClass, roughMatchedAspectList);
        Object proxy = ProxyCreator.createProxy(targetClass, aspectListExecutor);
        beanContainer.addBean(targetClass,proxy);
    }

    private List<AspectInfoPlus> collectRoughMatchedAspectListForSpecificClass(List<? extends AspectInfoPlus> aspectInfoList, Class<?> targetClass) {
        List<AspectInfoPlus> roughMatchedAspectList = new ArrayList<>();
        for (AspectInfoPlus aspectInfoPlus : aspectInfoList) {
//            // 只能校验within 使用表达式对象匹配一下看该类是否符合匹配条件，符合就加入集合
            if(aspectInfoPlus.getPointcutLocator().roughMatches(targetClass)){
                roughMatchedAspectList.add(aspectInfoPlus);
            }
        }
        return roughMatchedAspectList;
    }

    /**
     * 拼装AspectInfoPlusList
     * @param aspectSet 所有Aspect类
     * @return AspectInfoPlusList
     */
    private List<AspectInfoPlus> packAspectInfoList(Set<Class<?>> aspectSet) {
        List<AspectInfoPlus> aspectInfoPlusList = new ArrayList<>();
        for (Class<?> targetClass : aspectSet) {
            AspectPlus aspectPlusAnnotation = targetClass.getAnnotation(AspectPlus.class);
            Order orderAnnotation = targetClass.getAnnotation(Order.class);
            int orderValue = orderAnnotation.value();
            // 获取注解上写的表达式
            String pointcut = aspectPlusAnnotation.pointcut();
            // 创建表达式对象
            PointcutLocator pointcutLocator = new PointcutLocator(pointcut);
            // 根据class类从Ioc容器中获取实例
            DefaultAspect defaultAspect = (DefaultAspect) beanContainer.getBean(targetClass);
            // 创建AspectInfoPlus对象
            AspectInfoPlus aspectInfoPlus = new AspectInfoPlus(orderValue, defaultAspect, pointcutLocator);
            aspectInfoPlusList.add(aspectInfoPlus);
        }
        return aspectInfoPlusList;
    }

    /**
     * 3、按照不同的织入目标分别取按序织入Aspect的逻辑
     * @param category
     * @param aspectInfos
     */
    private void weaveByCategory(Class<? extends Annotation> category, List<AspectInfo> aspectInfos) {
        // 获取被代理类的集合
        Set<Class<?>> classSet = beanContainer.getClassesByAnnotation(category);
        if(ValidateUtil.isEmpty(classSet)) return;
        for (Class<?> targetClass : classSet) {
            // 创建动态代理对象
            AspectListExecutor aspectListExecutor = new AspectListExecutor(targetClass, aspectInfos);
            Object proxy = ProxyCreator.createProxy(targetClass, aspectListExecutor);
            // 使用代理对象取代未被代理前的类实例
            beanContainer.addBean(targetClass,proxy);
        }
    }

    /**
     * 2、将切面类按照不同的切入目标进行划分
     * @param categorizedMap 划分切面类的容器
     * @param aspectClass 具体切面类
     */
    private void categorizeAspect(HashMap<Class<? extends Annotation>, List<AspectInfo>> categorizedMap, Class<?> aspectClass) {
        Aspect aspectAnnotation = aspectClass.getAnnotation(Aspect.class);
        Order orderAnnotation = aspectClass.getAnnotation(Order.class);
        DefaultAspect aspect = (DefaultAspect) beanContainer.getBean(aspectClass);
        AspectInfo aspectInfo = new AspectInfo(orderAnnotation.value(), aspect);
        // 如果之前没在Map中，那么就说明是第一次加入进来
        if(!categorizedMap.containsKey(aspectAnnotation.value())){
            List<AspectInfo> aspectInfoList = new ArrayList<>();
            aspectInfoList.add(aspectInfo);
            categorizedMap.put(aspectAnnotation.value(),aspectInfoList);
        }else{
            // 之前在Map中就直接累加
            List<AspectInfo> aspectInfoList = categorizedMap.get(aspectAnnotation.value());
            aspectInfoList.add(aspectInfo);
        }
    }

    /**
     * 校验@Aspect标记的类是否合法
     * 1、Aspect类必须同时加上@Order和@Aspect
     * 2、必须继承DefaultAspect.class
     * 3、@Aspect的属性值不能是本身，避免死循环
     * @param aspectClass aspect类
     * @return 合法值
     */
    private boolean verifyAspect(Class<?> aspectClass) {
        return aspectClass.isAnnotationPresent(Order.class) &&
                aspectClass.isAnnotationPresent(AspectPlus.class) &&
                DefaultAspect.class.isAssignableFrom(aspectClass);
    }


}
