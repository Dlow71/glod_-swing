package com.carbon.util.SwingX.aop.aspect;

import com.carbon.util.SwingX.aop.PointcutLocator;

public  class AspectInfoTemplate {
    // 执行优先级
    private int orderIndex;
    // 切面模板
    private DefaultAspect aspectObject;
    // 每个表达式单独的pointcut解析器
    private PointcutLocator pointcutLocator;

    public int getOrderIndex() {
        return orderIndex;
    }

    public DefaultAspect getAspectObject() {
        return aspectObject;
    }

    public PointcutLocator getPointcutLocator() {
        return pointcutLocator;
    }
}
