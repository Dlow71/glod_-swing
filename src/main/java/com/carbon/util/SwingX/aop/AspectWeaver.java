package com.carbon.util.SwingX.aop;

import com.carbon.util.ValidateUtil;
import com.carbon.util.SwingX.aop.annotation.Aspect;
import com.carbon.util.SwingX.aop.annotation.AspectPlus;
import com.carbon.util.SwingX.aop.annotation.Order;
import com.carbon.util.SwingX.aop.aspect.AspectInfo;
import com.carbon.util.SwingX.aop.aspect.DefaultAspect;
import com.carbon.util.SwingX.ioc.BeanContainer;


import java.lang.annotation.Annotation;
import java.util.*;

public class AspectWeaver {
    private BeanContainer beanContainer;

    public AspectWeaver(){
        // 因为beanContain本身就是单例的，所以可以直接这样获取
        beanContainer = BeanContainer.getInstance();
    }

    public Map<Class<? extends Annotation>, List<AspectInfo>> doAop(){
        // 1、获取所有被@Aspect标记的切面类
        Set<Class<?>> aspectSet = beanContainer.getClassesByAnnotation(Aspect.class);
        if(ValidateUtil.isEmpty(aspectSet)){
            return new HashMap<>();
        }
        // 2、将切面类按照不同的切入目标进行划分
        HashMap<Class<? extends Annotation>, List<AspectInfo>> categorizedMap = new HashMap<>();
        for (Class<?> aspectClass : aspectSet) {
            // 2.1、判断切面类的合法性
            if(verifyAspect(aspectClass)){
                // 2.2、具体划分
                categorizeAspect(categorizedMap,aspectClass);
            }else{
                throw new RuntimeException(aspectClass.getSimpleName()+"切面类定义不合法，请检查以下4点\n" +
                        "1、切面类是否都加上@Aspect注解和@Order注解\n" +
                        "2、切面类是否继承DefaultAspect\n" +
                        "3、@Aspect的value是否定义的不是@Aspect本身\n" +
                        "4、@Aspect的value是否定义的不是二代龙神");
            }
        }
        if(ValidateUtil.isEmpty(categorizedMap)) return new HashMap<>();
        return categorizedMap;

        // 3、按照不同的织入目标分别取按序织入Aspect的逻辑(已经改成统一用AopExecutor去织入了)
//        for (Class<? extends Annotation> category : categorizedMap.keySet()) {
//            weaveByCategory(category,categorizedMap.get(category));
//        }

    }

    /**
     * 3、按照不同的织入目标分别取按序织入Aspect的逻辑
     * @param category
     * @param aspectInfos
     */
    private void weaveByCategory(Class<? extends Annotation> category, List<AspectInfo> aspectInfos) {
        // 获取被代理类的集合
        Set<Class<?>> classSet = beanContainer.getClassesByAnnotation(category);
        if(ValidateUtil.isEmpty(classSet)) return;
        for (Class<?> targetClass : classSet) {
            // 创建动态代理对象
            AspectListExecutor aspectListExecutor = new AspectListExecutor(targetClass, aspectInfos);
            Object proxy = ProxyCreator.createProxy(targetClass, aspectListExecutor);
            // 使用代理对象取代未被代理前的类实例
            beanContainer.addBean(targetClass,proxy);
        }
    }

    /**
     * 2、将切面类按照不同的切入目标进行划分
     * @param categorizedMap 划分切面类的容器
     * @param aspectClass 具体切面类
     */
    private void categorizeAspect(HashMap<Class<? extends Annotation>, List<AspectInfo>> categorizedMap, Class<?> aspectClass) {
        Aspect aspectAnnotation = aspectClass.getAnnotation(Aspect.class);
        Order orderAnnotation = aspectClass.getAnnotation(Order.class);
        DefaultAspect aspect = (DefaultAspect) beanContainer.getBean(aspectClass);
        AspectInfo aspectInfo = new AspectInfo(orderAnnotation.value(), aspect);
        // 如果之前没在Map中，那么就说明是第一次加入进来
        if(!categorizedMap.containsKey(aspectAnnotation.value())){
            List<AspectInfo> aspectInfoList = new ArrayList<>();
            aspectInfoList.add(aspectInfo);
            categorizedMap.put(aspectAnnotation.value(),aspectInfoList);
        }else{
            // 之前在Map中就直接累加
            List<AspectInfo> aspectInfoList = (List<AspectInfo>) categorizedMap.get(aspectAnnotation.value());
            aspectInfoList.add(aspectInfo);
        }
    }

    /**
     * 校验@Aspect标记的类是否合法
     * 1、Aspect类必须同时加上@Order和@Aspect
     * 2、必须继承DefaultAspect.class
     * 3、@Aspect的value不能是本身，避免死循环
     * 4、别value设置成二代龙神了，比如@Aspect(value=AspectPlus.class)
     * @param aspectClass aspect类
     * @return 合法值
     */
    private boolean verifyAspect(Class<?> aspectClass) {
        return aspectClass.isAnnotationPresent(Order.class) &&
                aspectClass.isAnnotationPresent(Aspect.class) &&
                DefaultAspect.class.isAssignableFrom(aspectClass) &&
                aspectClass.getAnnotation(Aspect.class).value()!=Aspect.class &&
                aspectClass.getAnnotation(Aspect.class).value()!=AspectPlus.class;
    }
}
