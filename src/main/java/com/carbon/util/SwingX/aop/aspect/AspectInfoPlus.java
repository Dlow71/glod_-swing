package com.carbon.util.SwingX.aop.aspect;

import com.carbon.util.SwingX.aop.PointcutLocator;

/**
 * 表达式匹配AOP
 */
public class AspectInfoPlus extends AspectInfoTemplate{
    private int orderIndex;
    private DefaultAspect aspectObject;
    // 每个表达式单独的pointcut解析器
    private PointcutLocator pointcutLocator;

    // AOP二代龙神启动！！
    public AspectInfoPlus(int orderIndex, DefaultAspect aspectObject, PointcutLocator pointcutLocator) {
        this.orderIndex = orderIndex;
        this.aspectObject = aspectObject;
        this.pointcutLocator = pointcutLocator;
    }


    public int getOrderIndex() {
        return orderIndex;
    }

    public DefaultAspect getAspectObject() {
        return aspectObject;
    }

    public PointcutLocator getPointcutLocator() {
        return pointcutLocator;
    }
}
