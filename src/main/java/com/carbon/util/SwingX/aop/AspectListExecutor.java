package com.carbon.util.SwingX.aop;

import com.carbon.util.ValidateUtil;
import com.carbon.util.SwingX.aop.aspect.AspectInfoTemplate;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AspectListExecutor implements MethodInterceptor {
    // 被代理的class对象
    private Class<?> targetClass;
    // 排序好的Aspect列表
    private List<? extends AspectInfoTemplate> sortedAspectInfoTemplateList;

    public AspectListExecutor(Class<?> targetClass, List<? extends AspectInfoTemplate> sortedAspectInfoTemplateList) {
        this.targetClass = targetClass;
        // 提前根据order值排好序
        this.sortedAspectInfoTemplateList = sortByOrder(sortedAspectInfoTemplateList);
    }

    // 根据order去排序Aspect
    private List<? extends AspectInfoTemplate> sortByOrder(List<? extends AspectInfoTemplate> sortedAspectInfoTemplateList) {
        Collections.sort(sortedAspectInfoTemplateList, new Comparator<AspectInfoTemplate>() {
            @Override
            public int compare(AspectInfoTemplate o1, AspectInfoTemplate o2) {
                return o1.getOrderIndex()-o2.getOrderIndex();
            }
        });
        return sortedAspectInfoTemplateList;
    }

    /**
     * 具体执行方法
     * @param proxy 动态代理对象
     * @param method 被代理方法实例
     * @param args 参数数组
     * @param methodProxy 代替Method的动态Method对象
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        Object returnValue = null;
        // TODO 这里其实少了一步精筛
        if(ValidateUtil.isEmpty(sortedAspectInfoTemplateList)){
            returnValue = methodProxy.invokeSuper(proxy,args);
            return returnValue;
        }
        // 1、按照order的顺序执行过链条里面的before方法
        invokeBeforeAdvices(method,args);
        try{
            // 2、执行被代理类的方法
            returnValue = methodProxy.invokeSuper(proxy,args);
            // 3、无异常就继续执行afterReturning
            returnValue = invokeAfterAdvices(method,args,returnValue);
        } catch (Exception e){
            // 4、如果代理方法抛异常，就轮到afterThrowing登场咯
            invokeAfterThrowingAdvices(method,args,e);
        }
        return returnValue;
    }

    // 4、按order顺序执行afterThrowing方法，优先级越高的越后执行，所以是倒着执行
    private void invokeAfterThrowingAdvices(Method method, Object[] args, Exception e) throws Throwable{
        for (int i = sortedAspectInfoTemplateList.size()-1; i >= 0; i--) {
            sortedAspectInfoTemplateList.get(i).getAspectObject().afterThrowing(targetClass,method,args,e);
        }
    }

    // 3、按order顺序执行after方法，优先级越高的越后执行，所以是倒着执行
    private Object invokeAfterAdvices(Method method, Object[] args, Object returnValue) throws Throwable{
        for (int i = sortedAspectInfoTemplateList.size()-1; i >= 0; i--) {
            returnValue = sortedAspectInfoTemplateList.get(i).getAspectObject().afterReturning(targetClass,method,args,returnValue);
        }
        return returnValue;
    }

    // 1、按照order顺序执行before方法
    private void invokeBeforeAdvices(Method method, Object[] args) throws Throwable{
        for (AspectInfoTemplate AspectInfoTemplate : sortedAspectInfoTemplateList) {
            AspectInfoTemplate.getAspectObject().before(targetClass,method,args);
        }
    }
}
