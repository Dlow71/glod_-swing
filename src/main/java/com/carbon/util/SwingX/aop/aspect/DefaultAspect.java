package com.carbon.util.SwingX.aop.aspect;

import java.lang.reflect.Method;

/**
 * 切面模板，是切面类需要继承的
 */
public abstract class DefaultAspect {
    /**
     * 事前拦截
     * @param targetClass 被代理的目标类
     * @param method  被代理的目标方法
     * @param args  被代理的目标方法对应的参数列表
     * @throws Throwable 所有异常的超类
     */
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable{

    }

    /**
     * 事后拦截
     * @param targetClass 被代理的目标类
     * @param method  被代理的目标方法
     * @param args  被代理的目标方法对应的参数列表
     * @param returnValue 方法返回的值
     * @return 处理后的方法返回值
     * @throws Throwable 所有异常的超类
     */
    public Object afterReturning(Class<?> targetClass,Method method,Object[] args,Object returnValue)throws Throwable{
        return returnValue;
    }

    /**
     * 抛出异常后处理
     * @param targetClass 被代理的目标类
     * @param method  被代理的目标方法
     * @param args  被代理的目标方法对应的参数列表
     * @param e 方法抛出的异常
     * @throws Throwable 所有异常的超类
     */
    public void afterThrowing(Class<?> targetClass,Method method,Object[] args,Throwable e)throws Throwable{

    }
}
