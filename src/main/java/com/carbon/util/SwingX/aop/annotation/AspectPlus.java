package com.carbon.util.SwingX.aop.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AspectPlus {
    // 现在使用的是AspectJ，表达式匹配，所以类型改成String
    String pointcut();
}
