package com.carbon.util.SwingX.aop.aspect;

/**
 * 注解匹配 AOP
 */
public class AspectInfo extends AspectInfoTemplate{
    private int orderIndex;
    private DefaultAspect aspectObject;

    public AspectInfo(int orderIndex, DefaultAspect aspectObject) {
        this.orderIndex = orderIndex;
        this.aspectObject = aspectObject;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public DefaultAspect getAspectObject() {
        return aspectObject;
    }

}
