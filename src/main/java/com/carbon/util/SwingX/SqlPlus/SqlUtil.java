package com.carbon.util.SwingX.SqlPlus;


import com.carbon.common.req.BaseRequest;
import com.carbon.util.StringUtils;
import com.carbon.util.SwingX.CamelCaseUtil;
import com.carbon.util.SwingX.ObjectUtils;
import com.carbon.util.SwingX.SqlPlus.annotation.Query;
import com.carbon.util.SwingX.CheckUtil;


import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * author 山沐与山
 */
public class SqlUtil {
    private static String prefixTableName = "test_";
    public static String getPreFixTableName(){
        return prefixTableName;
    }

    /**
     * 生成查询SQL
     * @param clazz
     * @param obj
     * @param initSql
     * @param <R>
     * @return
     */
    public static<R> StringBuilder genSelectSql(Class<?> clazz,R obj,String... initSql){
        if(!(obj instanceof BaseRequest)) throw new IllegalArgumentException("传入的类必须继承BaseRequest");
        StringBuilder sql = new StringBuilder();
        if(initSql.length==0){
            String[] suffix = clazz.getSimpleName().split("Request");
            if(suffix.length==0) throw new IllegalArgumentException("Request类后缀必须带有Request");
            sql.append("select * from "+prefixTableName+suffix[0].toLowerCase()+" where 1=1 ");
        }else{
            sql.append(initSql[0]);
        }
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            try {
                field.setAccessible(true);
                Object o = field.get(obj);
                // 如果对象为null或者内容为空，直接跳过
                if(o==null || "".equals(o.toString().trim())) continue;
                // 获取该属性的类型
                String typeName = field.getType().getSimpleName();
//                System.out.println(field.getName()+":"+type.getSimpleName());

                // 这里写的比较拉胯，是可以优化一下的
                switch (typeName){
                    case "long":
                    case "Long":
                    {
                        Long value = (Long) o;
                        // 检查一下看查询是否合法
                        if(CheckUtil.check(field,value)){
                            TypeGen(sql,value,field);
                        }else{
                            throw new IllegalArgumentException("参数值非法");
                        }
                        break;
                    }

                    case "String":
                    {
                        String value = (String) o;
                        if(CheckUtil.check(field,value)){
                            TypeGen(sql,value,field);
                        }else{
                            throw new IllegalArgumentException("参数值非法");
                        }
                        break;
                    }
                    case "Float":
                    case "float":
                    case "double":
                    case "Double":
                    {
                        Double value = (Double) o;
                        if(CheckUtil.check(field,value)){
                            TypeGen(sql,value,field);
                        }else{
                            throw new IllegalArgumentException("参数值非法");
                        }
                        break;
                    }
                    case "Integer":
                    case "int": {
                        Integer value = (Integer) o;
                        if(CheckUtil.check(field,value)){
                            TypeGen(sql,value,field);
                        }else{
                            throw new IllegalArgumentException("参数值非法");
                        }
                        break;
                    }
                    default: throw new IllegalArgumentException("传入的Request类只可为基础类型，当然也可以自己拓展");
                }
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
//            SysUser user = SecurityUtils.getUser();
//            System.out.println(user.getUserType()+"此次生成的SQL为"+sql.toString());
        }
        return sql;
    }


    /**
     * 生成修改SQL的PS对象
     * @param clazz
     * @param obj
     * @param initSql
     * @param conn
     * @param ps
     * @param splicing
     * @param <R>
     * @param <O>
     * @return
     */
    public static<R,O> PreparedStatement genUpdatePs(Class<?> clazz, R obj, StringBuilder initSql, Connection conn, PreparedStatement ps,O splicing){
        if(StringUtils.isBlank(initSql)){
            throw new IllegalArgumentException("初始sql不能为空，这个还是要给的");
        }
        if(splicing==null){
            throw new IllegalArgumentException("拼接SQL尾端类需要提交的，否则没有where全给你改了");
        }
        Field[] fields = clazz.getDeclaredFields();
        List<Entry> list = new LinkedList<>();
        // 拼接 ?
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object o = field.get(obj);
                if(o==null || "".equals(o.toString().trim())) continue;
                String fieldName = field.getName();
                fieldName = CamelCaseUtil.toUnderlineName(fieldName);
                initSql.append(fieldName+"=?,");
                list.add(new Entry(o.getClass().getSimpleName(),o));
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        initSql.deleteCharAt(initSql.length()-1);
        initSql.append(" where 1=1 ");
        // 拼接update后面的where条件
        Field[] splicingFields = splicing.getClass().getDeclaredFields();
        for (Field splicingField : splicingFields) {
            try {
                splicingField.setAccessible(true);
                Object o = splicingField.get(splicing);
                if(ObjectUtils.isEmpty(o)) continue;
                String key = CamelCaseUtil.toUnderlineName(splicingField.getName());
                initSql.append(" and "+key+"='"+o+"'");
                splicingField.setAccessible(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        // ps一个个set值进去
        try {
            ps = conn.prepareStatement(initSql.toString());
            int offset = 1;
            for (Entry entry:list) {
                String type = entry.getClazz();
                Object value = entry.getValue();
                switch (type){
                    case "Long":
                        Long Lv = (Long) value;
                        ps.setLong(offset++,Lv);
                        break;
                    case "String":
                        String Sv = (String) value;
                        ps.setString(offset++,Sv);
                        break;
                    case "int":
                    case "Integer":
                        Integer Iv = (Integer) value;
                        ps.setInt(offset++,Iv);
                        break;
                    case "float":
                    case "double":
                    case "Double":
                        Double Dv = (Double) value;
                        ps.setDouble(offset++,Dv);
                        break;
                    case "Date":
                        Date dateV = (Date) value;
                        ps.setTimestamp(offset++,new Timestamp(dateV.getTime()));
                        break;
                    default:throw new IllegalArgumentException("未知类型，可自行添加支持");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ps;
    }

    private static void TypeGen(StringBuilder sql, Object value,Field field) {
        Query annotation = field.getAnnotation(Query.class);
        if(annotation==null){ // 默认是等于
            sql.append(" and "+CamelCaseUtil.toUnderlineName(field.getName())+"='"+value+"'");
            return;
        }
        Query.Type type = annotation.type();
        switch (type) {
            case EQUAL:
                sql.append(" and "+CamelCaseUtil.toUnderlineName(field.getName())+"='"+value+"'");
                break;
            case LIKE:
                sql.append(" and "+CamelCaseUtil.toUnderlineName(field.getName())+" like '%"+value+"%'");
                break;
            case GREATER_THEN:
                sql.append(" and "+CamelCaseUtil.toUnderlineName(field.getName())+">"+value);
                break;
            case LESS_THEN:
                sql.append(" and "+CamelCaseUtil.toUnderlineName(field.getName())+"<"+value);
                break;
//            case IN:

        }
    }

    //获取子类以及父类所有的字段
    public static Field[] getAllFields(Class<?> clazz){
        ArrayList<Field> fieldList = new ArrayList<>();
        while(clazz!=null){
            fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
            clazz = clazz.getSuperclass();
        }
        Field[] fields = new Field[fieldList.size()];
        return fieldList.toArray(fields);
    }

    public static void main(String[] args) {
//        OrdersRequest ordersRequest = new OrdersRequest();
//        ordersRequest.setAuditStatus("正常");
//        ordersRequest.setOrderId(2L);
//        StringBuilder sql = SqlUtil.genSelectSql(OrdersRequest.class, ordersRequest);
//        System.out.println(sql.toString());

        LinkedHashMap<Class<?>, Object> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(String.class,"123");
        linkedHashMap.put(Integer.class,12);
        linkedHashMap.put(Long.class,1);

        for (Map.Entry<Class<?>, Object> entry : linkedHashMap.entrySet()) {
            Class<?> key = entry.getKey();
            String name = key.getSimpleName();
            Object value = entry.getValue();
            System.out.println(name+":"+value);
        }

        Object s1 = "123";
        Class<?> aClass = s1.getClass();

        Class<String> stringClass = String.class;
        String cast = stringClass.cast(s1);
        System.out.println(cast);
    }
}
class Entry{
    private String clazz;
    private Object value;

    public Entry(String clazz, Object value) {
        this.clazz = clazz;
        this.value = value;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
