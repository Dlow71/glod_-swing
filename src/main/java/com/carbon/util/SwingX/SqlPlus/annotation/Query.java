package com.carbon.util.SwingX.SqlPlus.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Query {
    // 默认查询类型
    Type type() default Type.EQUAL;
    // 禁止查的值
    String cannot() default "";

    enum Type{
        EQUAL,
        LIKE,
        GREATER_THEN,
        LESS_THEN,
    }
}
