package com.carbon.util.SwingX;

import com.carbon.util.SwingX.aop.*;
import com.carbon.util.SwingX.ioc.BeanContainer;
import com.carbon.util.SwingX.ioc.DependencyInjector;

@SuppressWarnings(value = "all")
public class SwingX {
    private static BeanContainer beanContainer;

    static{
        // 获取安全的单例bean容器
        beanContainer = BeanContainer.getInstance();
    }

    /**
     * 指定类 初始化该类路径及其子路径下的所有bean
     * @param mainClass 启动类的class类
     */
    public static void initialize(Class<?> mainClass){
        String packageName = mainClass.getName();
        packageName = packageName.substring(0,packageName.lastIndexOf("."));
        execute(packageName);
    }

    /**
     * 指定包名 初始化该包及其子包下的所有bean
     * @param packageName
     */
    public static void initialize(String packageName){
        execute(packageName);
    }

    /**
     * 执行
     * @param packageName
     */
    private static void execute(String packageName){
        // IOC容器初始化--加载bean
        beanContainer.loadBeans(packageName);

        // 统一AOP执行器
        new AopExecutor().executeWeave();

        // 依赖注入
        new DependencyInjector().doIoc();
    }



}


