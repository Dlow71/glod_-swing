package com.carbon.util.SwingX;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * 反射工具类
 */
public class ClassUtil {
    private final static Logger log = LoggerFactory.getLogger(ClassUtil.class);
    public static final String FILE_PROTOCOL = "file";

    /**
     * 获取指定包下的所有class类
     * @param packageName 包路径
     * @return class的Set集合
     */
    public static Set<Class<?>> fetchPackageClass(String packageName){
        // 获取类加载器
        ClassLoader classLoader = getClassLoader();
        // URL获取资源分隔符必须是 /
        URL url = classLoader.getResource(packageName.replace(".", "/"));
        if(url==null){
            log.warn("从package中获取不到资源，包路径有问题->"+packageName);
            return null;
        }
        Set<Class<?>> classSet = null;
        // 只有是File类型才继续
        if(url.getProtocol().equalsIgnoreCase(FILE_PROTOCOL)){
            classSet = new HashSet<>();
            // 此时还不确定是file还是文件夹
            File file = new File(url.getPath());
            // 递归去获取class
            FetchFileClass(classSet,file,packageName);
        }
        return classSet;
    }

    /**
     * 获取文件夹下的所有class
     * @param classSet
     * @param packageFile
     * @param packageName
     */
    private static void FetchFileClass(Set<Class<?>> classSet, File packageFile, String packageName) {
        // 如果不是文件夹，直接返回
        if(!packageFile.isDirectory()) return;
        // 是文件夹的话继续筛选该文件夹里的文件夹出来
        File[] files = packageFile.listFiles(new FileFilter() {
            // 具体筛选文件的方法
            @Override
            public boolean accept(File file) {
                // 是文件夹就留下 因为文件夹后面还要拿去遍历里面的文件
                if(file.isDirectory()){
                    return true;
                }else{ // 不是文件夹就说明是文件，看该文件是不是我们想要的
                    // 获取文件绝对路径
                    String absolutePath = file.getAbsolutePath();
                    // 由于JF还会多生成几个class类，所以需要排除掉
                    if(absolutePath.endsWith(".class") && !absolutePath.contains("$")){
                        // 是class文件就加载
                        loadToClassSet(absolutePath);
                    }
                }
                return false;
            }

            /**
             * 根据绝对路径加载class类到set集合中
             * @param absolutePath 类的绝对路径
             */
            private void loadToClassSet(String absolutePath){
                // 将路径处理成如右 com.xxx.demo
                absolutePath = absolutePath.replace(File.separator,".");
                String className = absolutePath.substring(absolutePath.indexOf(packageName));
                // 把尾巴 .class 砍掉
                className = className.substring(0,className.lastIndexOf("."));
                // 加载类并放入set集合
                Class<?> clazz = loadClass(className);
                classSet.add(clazz);
            }
        });

        // 继续遍历子文件夹
        // 增强for碰到null是会出异常的，所以手动判断一下
        if(files!=null){
            for (File file : files) {
                // 继续递归找文件
                FetchFileClass(classSet,file,packageName);
            }
        }
    }


    /**
     * 获取ClassLoader
     */
    public static ClassLoader getClassLoader(){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader;
    }

    /**
     * 根据 路径+类名 加载类
     */
    public static Class<?> loadClass(String className){
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("根据路径+类名"+className+"加载类失败",e);
            throw new RuntimeException();
        }
    }

    /**
     * 根据class类实例化出对象并返回
     */
    public static <T> T newInstance(Class<T> clazz){
        try {
            Constructor<?> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            T obj = (T)constructor.newInstance();
            constructor.setAccessible(false);
            return obj;
        } catch (Exception e) {
            log.error("实例化class错误",e);
            throw new RuntimeException(e);
        }
    }

    /**
     * 为某个对象里的某个字段设置值
     * @param field 字段
     * @param value 值
     * @param target 设置目标
     */
    public static<T,V> void setField(Field field,T target,V value){
        field.setAccessible(true);
        try {
            field.set(target,value);
        } catch (IllegalAccessException e) {
            log.error("注入异常",e);
        } finally {
            field.setAccessible(false);
        }
    }

    public static void main(String[] args) {
        fetchPackageClass("com.carbon.main");
    }
}
