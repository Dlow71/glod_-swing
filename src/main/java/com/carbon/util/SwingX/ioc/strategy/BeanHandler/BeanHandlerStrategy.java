package com.carbon.util.SwingX.ioc.strategy.BeanHandler;

public interface BeanHandlerStrategy {
    public void handler(Class<?> clazz,Object instance);
}
