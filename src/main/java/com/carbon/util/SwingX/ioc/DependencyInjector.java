package com.carbon.util.SwingX.ioc;

import com.carbon.common.handler.ProjectHandler;
import com.carbon.main.ProjectMain;
import com.carbon.util.StringUtils;
import com.carbon.util.ValidateUtil;
import com.carbon.util.SwingX.ClassUtil;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Set;

/**
 * 依赖注入类
 */
public class DependencyInjector {
    private final static Logger log = LoggerFactory.getLogger(BeanContainer.class);
    private static boolean finishFlag = false;

    public static boolean getFinishFlag(){
        return finishFlag;
    }

    private BeanContainer beanContainer;

    public DependencyInjector(){
        this.beanContainer = BeanContainer.getInstance();
    }

    public void doIoc(){
        Set<Class<?>> classes = beanContainer.getClasses();
        if(ValidateUtil.isEmpty(classes)){
            log.warn("当前容器是空的");
            return;
        }
        // 遍历容器所有对象
        for (Class<?> clazz : classes) {
            // 获取对象中所有属性
            Field[] fields = clazz.getDeclaredFields();
            if(ValidateUtil.isEmpty(fields)){
                continue;
            }
            for (Field field : fields) {
                // 如果发现属性上加了@Autowired注解，就尝试注入
                if(field.isAnnotationPresent(Autowired.class)){
                    Autowired annotation = field.getAnnotation(Autowired.class);
                    // 获取指定name
                    String autowiredValue = annotation.value();
                    // 获取该属性的类型
                    Class<?> fieldClass = field.getType();
                    // 根据类型或者name获取实例对象
                    Object injectObj = getFieldInstance(fieldClass,autowiredValue);
                    if(injectObj==null){ // 为空就说明该对象没被注入IOC容器
                        throw new RuntimeException("注入失败，该对象是"+field.getName()+"，Autowired的value值为"+autowiredValue);
                    }else{
                        // 对象不为空就设置进去
                        Object targetBean = beanContainer.getBean(clazz);
                        ClassUtil.setField(field,targetBean,injectObj);
                    }
                }
            }
        }
        finishFlag = true;
    }

    private Object getFieldInstance(Class<?> fieldClass, String autowiredValue) {
        // 直接根据类型去找
        Object fieldBean = beanContainer.getBean(fieldClass);
        if(fieldBean!=null){
            return fieldBean;
        }else{ // 类型找不到就说明要注入的是实现类
            Class<?> clazz = getImplClass(fieldClass,autowiredValue);
            if(clazz!=null){
                Object bean = beanContainer.getBean(clazz);
                if(bean==null)
                throw new RuntimeException("注入失败，该对象是"+fieldClass.getSimpleName()+"，Autowired的value值为"+autowiredValue);
                return bean;
            }
        }
        return null;
    }

    /**
     * 获取实现类
     * @param fieldClass
     * @return
     */
    private Class<?> getImplClass(Class<?> fieldClass, String autowiredValue) {
        // 获取该类下的所有实现类
        Set<Class<?>> classesBySuper = beanContainer.getClassesBySuper(fieldClass);
        if(!ValidateUtil.isEmpty(classesBySuper)){
            if(StringUtils.isBlank(autowiredValue)){ // 如果没有指定name，就直接返回第一个，因为本身就是根据类找的
                if(classesBySuper.size()>1){
                    //如果多于两个实现类且用户并未指定其中一个实现类，则抛出异常
                    throw new RuntimeException("你这要注入依赖的类："+fieldClass+"有多个实现类，但是你又不告诉具体注入哪个类，建议在@Autowired注解里面为value赋值，告诉我要注入哪个类");
                }else if(classesBySuper.size()==1){
                    return classesBySuper.iterator().next();
                }
            }else{ // 指定了name的话，就根据实例名去找
                for (Class<?> clazz : classesBySuper) {
                    // TODO 这里其实还应该去遍历下nameContainer
                    if(clazz.getSimpleName().equals(autowiredValue)){
                        return clazz;
                    }
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        BeanContainer instance = BeanContainer.getInstance();
        instance.loadBeans("com.carbon");
        new DependencyInjector().doIoc();
        ProjectHandler projectHandler = (ProjectHandler) instance.getBean(ProjectHandler.class);
        ProjectMain projectMain = (ProjectMain) instance.getBean(ProjectMain.class);
        System.out.println();
    }
}
