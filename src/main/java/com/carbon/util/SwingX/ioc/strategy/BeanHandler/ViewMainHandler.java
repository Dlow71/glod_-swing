package com.carbon.util.SwingX.ioc.strategy.BeanHandler;

import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ViewMainHandler implements BeanHandlerStrategy{
    @Override
    public void handler(Class<?> clazz,Object instance) {
        ViewMain viewMain = clazz.getAnnotation(ViewMain.class);
        // 这里其实有点写死了，只支持JFrame，以后再适配别的
        DefaultFrame frame = (DefaultFrame) instance;
        // 每次窗口打开都自动刷新数据 同时检查按钮是否绑定过事件
        frame.addWindowListener(new WindowAdapter() {
            // 这个事件本身就很诡异 setVisible(true)一次后，往后面都不会再触发了
            @Override
            public void windowOpened(WindowEvent e) {
                // 避免触发多次，windowOpened这个事件比较诡异，只用第一次setVisible(true)的时候调用就好了
                // 已经改成重写DefaultFrame的setVisible方法，放在这里加载数据有问题，不靠谱啊
//                if(!frame.getLoadData()){
//                    frame.loadData();
//                    frame.setLoadData(true);
//                }
                // 绑定按钮只需要绑定一次!!! 绑定多次的话会出现很诡异的事情
                if(!frame.getBindAction()){
                    // 执行绑定按钮操作
                    frame.bindAction();
                    // 标识该组件已经绑定过按钮了
                    frame.setBindAction(true);
                }

            }

//            @Override
//            public void windowClosed(WindowEvent e) {
//                if(BeanContainer.getInstance().isLoad())
//                frame.loadData();
//            }
        });
        // 如果该窗口不是开场窗口，如登录窗口或主页窗口，就dispose关闭窗口，避免占用图形资源
        if(!viewMain.isCurtain())
            frame.dispose();
    }
}
