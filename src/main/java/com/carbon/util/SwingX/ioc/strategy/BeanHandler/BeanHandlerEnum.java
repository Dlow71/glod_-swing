package com.carbon.util.SwingX.ioc.strategy.BeanHandler;

import com.carbon.util.SwingX.ioc.annotation.ViewMain;

import java.lang.annotation.Annotation;

public enum BeanHandlerEnum {
    VIEW_MAIN_HANDLER(ViewMain.class,new ViewMainHandler());
    private Class<? extends Annotation> beanAnnotation;
    private BeanHandlerStrategy strategy;

    BeanHandlerEnum(Class<? extends Annotation> beanAnnotation, BeanHandlerStrategy strategy) {
        this.beanAnnotation = beanAnnotation;
        this.strategy = strategy;
    }

    public static BeanHandlerStrategy getStrategy(Class<? extends Annotation> beanAnnotation){
        BeanHandlerEnum[] values = values();
        for (BeanHandlerEnum value : values) {
            if(value.beanAnnotation==beanAnnotation){
                return value.strategy;
            }
        }
        return null;
    }

}
