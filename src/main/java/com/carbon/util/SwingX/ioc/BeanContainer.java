package com.carbon.util.SwingX.ioc;



import com.carbon.util.ValidateUtil;
import com.carbon.util.SwingX.ClassUtil;
import com.carbon.util.SwingX.aop.annotation.Aspect;
import com.carbon.util.SwingX.aop.annotation.AspectPlus;
import com.carbon.util.SwingX.ioc.annotation.Component;
import com.carbon.util.SwingX.ioc.annotation.Handler;
import com.carbon.util.SwingX.ioc.annotation.Service;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;
import com.carbon.util.SwingX.ioc.strategy.BeanHandler.BeanHandlerEnum;
import com.carbon.util.SwingX.ioc.strategy.BeanHandler.BeanHandlerStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class BeanContainer {
    private final static Logger log = LoggerFactory.getLogger(BeanContainer.class);
    // 是否加载过的标记
    private boolean loadFlag = false;

    // 加载bean的注解列表  这里还可以优化，手写一个元注解，元注解标记到的注解，也可以被识别成bean，实现动态注解列表
    private static final List<Class<? extends Annotation>> BEAN_ANNOTATION
            = Arrays.asList(Service.class, Component.class, Handler.class, ViewMain.class, Aspect.class, AspectPlus.class);

    public static List<Class<? extends Annotation>> getBeanAnnotation(){
        return BEAN_ANNOTATION;
    }

    // 私有化构造器，保护容器安全
    private BeanContainer() {
    }

    /**
     * 使用枚举进一步保护容器
     * 枚举本身就是单例模式，且不允许通过new来创建枚举，就算使用反射获取出枚举也无法new
     * 同时反编译里面也可以看到内部值是直接在static块里面就创建出来了，因此就是单例
     * */
    private enum ContainerGuardian{
        GUARDIAN;
        private BeanContainer instance;
        ContainerGuardian(){
            instance = new BeanContainer();
        }
    }

    // 获取容器安全的单例对象
    public static BeanContainer getInstance(){
        return ContainerGuardian.GUARDIAN.instance;
    }

    public boolean isLoad(){
        return loadFlag;
    }

    // 存储 类型:值 容器
    private final ConcurrentHashMap<Class<?>,Object> classContainer = new ConcurrentHashMap<>();
    // 存储 名字:值 容器
    private ConcurrentHashMap<String,Object> nameContainer = new ConcurrentHashMap<>();

    /**
     * 扫描加载所有的Bean
     * @param packageName 指定包名
     */
    public synchronized void loadBeans(String packageName){
        if(isLoad()){
            log.warn("容器已经加载过了");
            return;
        }
        // 获取包下及其子包所有的class类
        Set<Class<?>> classSet = ClassUtil.fetchPackageClass(packageName);
        if(ValidateUtil.isEmpty(classSet)){
            log.warn("从package下面提取不到任何东西:"+packageName);
        }
        for (Class<?> clazz : classSet) {
            for (Class<? extends Annotation> annotation : BEAN_ANNOTATION) {
                // 判断这些类是否加了指定注解，只有注解标识的才可以被加载进入ioc容器
                if(clazz.isAnnotationPresent(annotation)){
                    // 使用反射根据class类创建出实例，此时的对象里面是还没有自动注入的
                    Object instance = ClassUtil.newInstance(clazz);
                    // 将对象注入到IOC容器中
                    classContainer.put(clazz,instance);

                    // 策略模式 有些注解标志的类是需要通过特殊处理的，如@ViewMain
                    // 获取该注解的策略
                    BeanHandlerStrategy strategy = BeanHandlerEnum.getStrategy(annotation);
                    // 策略不存在就不管了
                    if(strategy==null) continue;
                    // 策略存在则执行策略
                    strategy.handler(clazz,instance);
                }
            }
        }
        // 标识当前容器已经加载过了
        loadFlag = true;
    }

    /**
     * 添加一个class对象以及对应的实例
     * @param clazz class类
     * @param bean 实例
     * @return 原有的bean实例，没有就是返回null
     */
    public Object addBean(Class<?> clazz,Object bean){
        return classContainer.put(clazz,bean);
    }

    /**
     * 添加一个name以及对应的实例
     * @param name bean的名字
     * @param bean 实例
     * @return 原有的bean实例，没有就是返回null
     */
    public Object addBean(String name,Object bean){
        return nameContainer.put(name,bean);
    }

    /**
     * 指定删除容器中的实例与class对象
     * @param clazz
     * @return
     */
    public Object removeBean(Class<?> clazz){
        return classContainer.remove(clazz);
    }

    /**
     * 根据name指定删除容器中的实例
     * @param name
     * @return
     */
    public Object removeBean(String name){
        return nameContainer.remove(name);
    }

    /**
     * 根据Class对象获取bean实例
     * @param clazz
     * @return
     */
    public Object getBean(Class<?> clazz){
        return classContainer.get(clazz);
    }

    /**
     * 根据name获取bean实例
     * @param name
     * @return
     */
    public Object getBean(String name){
        return nameContainer.get(name);
    }

    /**
     * 根据name获取bean实例并删除该实例
     */
    public Object takeBean(String name){
        Object target = nameContainer.remove(name);
        return target;
    }


    /**
     * 获取容器管理的所有Class对象集合
     * @return
     */
    public Set<Class<?>> getClasses(){
        return classContainer.keySet();
    }

    /**
     * 获取容器管理的所有Name集合
     * @return
     */
    public Set<String> getNameSet(){
        return nameContainer.keySet();
    }

    /**
     * 获取带有指定注解的class对象集合
     * @param annotation
     * @return
     */
    public Set<Class<?>> getClassesByAnnotation(Class<? extends Annotation> annotation){
        Set<Class<?>> keySet = getClasses();
        if(ValidateUtil.isEmpty(keySet)){
            log.warn("容器中什么都没有");
            return null;
        }
        Set<Class<?>> classSet = new HashSet<>();
        for (Class<?> clazz : keySet) {
            if(clazz.isAnnotationPresent(annotation)){
                classSet.add(clazz);
            }
        }
        return classSet.size()>0 ? classSet:null;
    }

    /**
     * 根据接口或者父类获取实现类或者子类的集合，但是不包括其本身
     * @param interfaceOrClass 接口或者父类class
     * @return class集合
     */
    public Set<Class<?>> getClassesBySuper(Class<?> interfaceOrClass){
        Set<Class<?>> keySet = getClasses();
        if(ValidateUtil.isEmpty(keySet)){
            log.warn("容器中什么都没有");
            return null;
        }
        Set<Class<?>> classSet = new HashSet<>();
        for (Class<?> clazz : keySet) {
            // 找实现类，不能包括本身
            if(interfaceOrClass.isAssignableFrom(clazz) && !clazz.equals(interfaceOrClass)){
                classSet.add(clazz);
            }
        }
        return classSet.size()>0 ? classSet:null;
    }

    /**
     * 根据接口或者父类获取实现类或者子类的数量，但是不包括其本身
     * @param interfaceOrClass 接口或者父类class
     * @return class集合
     */
    public Integer getClassesCountBySuper(Class<?> interfaceOrClass){
        Set<Class<?>> keySet = getClasses();
        if(ValidateUtil.isEmpty(keySet)){
            return 0;
        }
        Integer count = 0;
        for (Class<?> clazz : keySet) {
            if(interfaceOrClass.isAssignableFrom(clazz) && !clazz.equals(interfaceOrClass)){
                count++;
            }
        }
        return count;
    }
}
