package com.carbon.util.SwingX.SwingAdapt;

import com.carbon.util.SwingX.ioc.BeanContainer;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;

/**
 * IOC适配Swing
 * 这里之所以选用抽象类，是因为抽象类不是强制实现方法，就算需要强制实现方法，直接修饰方法为abstract就行
 *
 * TODO 这里后序可以改成继承JFrame的顶级父类，然后创建DefaultJFrame，DefaultJDialog等，就可以继续适配其他窗口了
 */
public abstract class DefaultFrame extends JFrame {
    // 标记是否已经绑定过事件
    private boolean isBindAction = false;
    // 标记是否已经加载过数据
    private boolean isLoadData = false;

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // 会先执行父类的构造器，所以如果有重复写的JFrame方法其实是可以加在这里面，设置大小的方法除外！！
    public DefaultFrame() throws HeadlessException {
        setVisible(false);
        setResizable(true);
    }

    public void loadData(){
    }

    public void bindAction(){
    }

//    @Override
//    public void dispose() {
//        super.dispose();
//        // 必须先等IOC容器加载完，否则会出现空指针异常
//        if(BeanContainer.getInstance().isLoad()) loadData();
//    }

    // 暂时禁止重写该方法
    @Override
    public final void setVisible(boolean flag){
        super.setVisible(flag);
        // 必须先等IOC容器加载完，否则会出现java.lang.reflect.InvocationTargetException异常
        // 这个异常实际上是用反射实例化对象产生的异常，然后这个异常里面包装的其实是空指针异常，要具体调试去看
        // 那为什么会报空指针异常呢？主要是因为这里都是继承的DefaultFrame类，构造器中调用了setVisible方法
        // 但是这个方法我们已经重写了，此时直接去调用loadData的话就会出现空指针异常，因此还没有完成依赖注入,service为null
        // 只有IOC容器初始化完成 且 打开窗口的时候才更新数据
        if(BeanContainer.getInstance().isLoad() && flag)
        loadData();
    }


    public boolean getBindAction() {
        return isBindAction;
    }

    public void setBindAction(boolean bindAction) {
        isBindAction = bindAction;
    }

    public boolean getLoadData() {
        return isLoadData;
    }

    public void setLoadData(boolean loadData) {
        isLoadData = loadData;
    }
}
