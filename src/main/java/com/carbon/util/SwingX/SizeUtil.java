package com.carbon.util.SwingX;

import com.carbon.main.EditProjectMain;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Field;

/**
 * 快速给swing中的组件设置一个通用大小
 * 避免一个个set造成代码冗余
 */
public class SizeUtil {
    public static Font font = new Font("",Font.BOLD,20);

    public static void setSize(Class<?> clazz, EditProjectMain obj){
        Field[] fields = clazz.getDeclaredFields();
        try{
            for (Field field : fields) {
                field.setAccessible(true);
                if(field.getType() == JTextField.class){
                    JTextField textField = (JTextField) field.get(obj);
                    textField.setPreferredSize(new Dimension(249,25));
                    ClassUtil.setField(field,obj,textField);
                }else if(field.getType() == JLabel.class){
                    JLabel jLabel = (JLabel) field.get(obj);
                    jLabel.setFont(font);
                    ClassUtil.setField(field,obj,jLabel);
                }else if(field.getType() == JButton.class){
                    JButton jButton = (JButton) field.get(obj);
                    jButton.setFont(font);
                    ClassUtil.setField(field,obj,jButton);
                }
                field.setAccessible(false);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static<R> void setSize(Class<?> clazz,R obj){
        Field[] fields = clazz.getDeclaredFields();
        try{
            for (Field field : fields) {
                field.setAccessible(true);
                if(field.getType() == JTextField.class){
                    JTextField textField = (JTextField) field.get(obj);
                    textField.setPreferredSize(new Dimension(600,25));
                    ClassUtil.setField(field,obj,textField);
                }else if(field.getType() == JLabel.class){
                    JLabel jLabel = (JLabel) field.get(obj);
                    jLabel.setFont(font);
                    ClassUtil.setField(field,obj,jLabel);
                }else if(field.getType() == JButton.class){
                    JButton jButton = (JButton) field.get(obj);
                    jButton.setFont(font);
                    ClassUtil.setField(field,obj,jButton);
                }
                field.setAccessible(false);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
