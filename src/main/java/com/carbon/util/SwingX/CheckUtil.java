package com.carbon.util.SwingX;

import cn.hutool.core.util.NumberUtil;
import com.carbon.util.DialogUtil;
import com.carbon.util.StringUtils;
import com.carbon.util.SwingX.SqlPlus.annotation.Query;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * beta版本，目前功能性弱
 */
public class CheckUtil {
//    public static<R> void checkParam(Class<?> clazz,R obj){
//        Field[] fields = clazz.getDeclaredFields();
//        for (Field field : fields) {
//
//        }
//    }

    public static void main(String[] args) {
        checkNull("123",123);
    }

    public static boolean checkDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //设置为严格模式
            format.setLenient(false);
            format.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static boolean checkNull(Object... objects){
        for (Object object : objects) {
            if(object==null){
                DialogUtil.errorDialog("值不可为空");
                return false;
            }

            if(object instanceof String){
                if(StringUtils.isBlank(object.toString().trim())){
                    DialogUtil.errorDialog("值不可为空");
                    return false;
                }
            }
            if(object instanceof Integer || object instanceof Long || object instanceof Double){
                if(object==null || !NumberUtil.isNumber((CharSequence) object)){
                    DialogUtil.errorDialog("该填数字的地方要填数字！");
                    return false;
                }
            }

//            if(object instanceof Date){
//                Date date = (Date) object;
//                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                format.parse(date);
//            }
        }
        return true;
    }

    public static boolean check(Field field,Long value){
        return checkQuery(field,value);
    }

    public static boolean check(Field field,Integer value){
        return checkQuery(field,value);
    }

    public static boolean check(Field field,Double value){
        return checkQuery(field,value);
    }

    public static boolean check(Field field,String value){
        return checkQuery(field,value);
    }

    public static boolean checkQuery(Field field,Object obj){
        Query annotation = field.getAnnotation(Query.class);
        if(annotation==null) {
            return true;
        }
        // 看参数值是否包含禁止的值
        String cannot = annotation.cannot();
        if(!"".equals(cannot.trim())){
            if(cannot.equals(obj.toString()))
            throw new IllegalArgumentException(field.getName()+"参数值不能为"+cannot);
        }
        return true;
    }

}