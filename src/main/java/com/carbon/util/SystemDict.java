package com.carbon.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class SystemDict {
    //用户名称
    public static Map<String,String> userMap;
    //审批状态
    public static Map<String,String> auditStatus;
    //提交状态
    public static Map<String,String> applyStatus;
    //采购申请类型
    public static Map<String,String> applyType;
    //送货方式
    public static Map<String,String> deliveryType;
    //供应商类型
    public static Map<String,String> supplierType;
    //供应商等级
    public static Map<String,String> supplierLevel;
    //物料分类
    public static Map<String,String> materialType;
    //入库单
    public static Map<String,String> inWareHouseStatus;
    //wms_apply_type 仓库类型
    public static Map<String,String> wmsApplyType;
    //out_warehouse_status 出库状态
    public static Map<String,String> outWarehouseStatus;
    //scm_in_warehouse_status 供应链入库
    public static Map<String,String> scmInWarehouseStatus;
    //scm_out_warehouse_status 供应链出库
    public static Map<String,String> scmOutWarehouseStatus;
    //sale_plan_type 销售计划类型
    public static Map<String,String> salePlanType;
    //采购申请
    public static Map<String,String> purchaseApplyType;

    //project_status项目状态
    public static Map<String,String> projectStatus;
    //equipment_status 设备状态
    public static Map<String,String> equipmentStatus;

    //质量检查
    public static Map<String,String> mesCheckType;

    //schedule_requisition_status 排产领料状态
    public static Map<String,String> sheduleRequisitionStatus;
    //product_schedule_status 排产状态
    public static Map<String,String> productScheduleStatus;
    //event_type 事务类型
    public static Map<String,String> eventType;
    //contract_type 合同类型
    public static Map<String,String> contractType;
    //in_warehouse_status 入库单状态
    public static Map<String,String> inWarehouseStatus;
    // wms_replenish_status 补货单状态
    public static Map<String,String> wmsReplenishStatus;
    //receive_material_status 领料状态
    public static Map<String,String> receiveMaterialStatus;
    //product_job_status 生产作业状态
    public static Map<String,String> productJobStatus;
    //product_finish_status 生产完成状态
    public static Map<String,String> productFinishStatus;
    //mes_check_result 生产检查结果
    public static Map<String,String> mesCheckResult;
    //wms_check_type 仓库盘点类型
    public static Map<String,String> wmsCheckType;
    //mes_mistake_type 生产错误类型
    public static Map<String,String> mesMistakeType;
    //scm_customer_type 客户分类
    public static Map<String,String> scmCustomerType;
    //scm_customer_leve 客户信用等级
    public static Map<String,String> scmCustomerLevel;
    //valid_status 客户信用等级
    public static Map<String,String> validStatus;


    static {
        userMap=new HashMap<>();
        getUserName();
        Map<String,Map<String,String>> sysDict=getSystemDict();
        validStatus=sysDict.get("valid_status");
        auditStatus=sysDict.get("audit_status");
        applyStatus=sysDict.get("apply_status");
        applyType=sysDict.get("purchase_apply_type");
        deliveryType=sysDict.get("scm_delivery_type");
        supplierType=sysDict.get("scm_supplier_type");
        supplierLevel=sysDict.get("scm_supplier_level");
        materialType=sysDict.get("material_type");
        inWareHouseStatus=sysDict.get("in_warehouse_status");
        wmsApplyType=sysDict.get("wms_apply_type");
        outWarehouseStatus=sysDict.get("out_warehouse_status");
        scmInWarehouseStatus=sysDict.get("scm_in_warehouse_status");
        scmOutWarehouseStatus=sysDict.get("scm_out_warehouse_status");
        salePlanType=sysDict.get("sale_plan_type");
        purchaseApplyType=sysDict.get("purchase_apply_type");
        projectStatus=sysDict.get("project_status");
        equipmentStatus=sysDict.get("equipment_status");
        mesCheckType=sysDict.get("mes_check_type");
        sheduleRequisitionStatus=sysDict.get("schedule_requisition_status");
        productScheduleStatus=sysDict.get("product_schedule_status");
        eventType = sysDict.get("event_type");
        contractType = sysDict.get("contract_type");
        inWarehouseStatus = sysDict.get("in_warehouse_status");
        receiveMaterialStatus = sysDict.get("receive_material_status");
        productJobStatus = sysDict.get("product_job_status");
        wmsReplenishStatus = sysDict.get("wms_replenish_status");
        productFinishStatus=sysDict.get("product_finish_status");
        mesCheckResult=sysDict.get("mes_check_result");
        wmsCheckType=sysDict.get("wms_check_type");
        mesMistakeType=sysDict.get("mes_mistake_type");
        scmCustomerType=sysDict.get("scm_customer_type");
        scmCustomerLevel=sysDict.get("scm_customer_level");
    }

    /**
     * 数据字典
     * @return
     */
    private static Map<String,Map<String,String>> getSystemDict(){
        Map<String,Map<String,String>> sysDict=new HashMap<>();
        try(Connection conn=DBUtil.getConnection()){
            String sql="select dict_value,dict_label,dict_type from sys_dict_data where status=0";
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            while (rs.next()){
                //
                String key=rs.getString(1);
                //显示内容
                String context=rs.getString(2);
                //字典类型
                String type=rs.getString(3);
                if(sysDict.get(type)!=null){
                    Map<String,String> map=sysDict.get(type);
                    map.put(key,context);
                }else{//第一次添加
                    Map<String,String> map=new HashMap<>();
                    map.put(key,context);
                    sysDict.put(type,map);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return sysDict;
    }


    private static void getUserName(){
        try(Connection conn=DBUtil.getConnection()){
            String sql="select user_id,user_name from sys_user";
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            while (rs.next()){
                userMap.put(rs.getInt("user_id")+"",rs.getString("user_name"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
