package com.carbon.exception;

public class CarbonException extends RuntimeException{
    public CarbonException(String msg){
        super(msg);
    }
}
