/*
 * Created by JFormDesigner on Fri Jun 09 18:13:24 CST 2023
 */

package com.carbon.main;

import com.carbon.common.domain.Project;
import com.carbon.common.handler.ProjectHandler;
import com.carbon.common.res.TableDTO;
import com.carbon.common.service.project.ProjectService;
import com.carbon.util.SwingX.SizeUtil;
import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.BeanContainer;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;
import lombok.Getter;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Nu'yo'a'h
 */
@ViewMain
@Getter
public class EditProjectMain extends DefaultFrame {
    private static Vector<Object> combobox = new Vector<>();
    private BeanContainer beanContainer = BeanContainer.getInstance();

    @Autowired
    private ProjectHandler projectHandler;
    @Autowired
    private ProjectService projectService;

    
    {
        combobox.addElement("挂起");
        combobox.addElement("禁用");
        combobox.addElement("正常");
    }
    
    public EditProjectMain() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                dispose();
            }
        });
        initComponents();
        SizeUtil.setSize(this.getClass(),this);
        pack();
        // 居中必须放在pack()的后面，不然先居中的话，然后再pack改变组件大小就歪了
        setLocationRelativeTo(null);
    }

    @Override
    public void loadData() {
        Long projectId = (Long) beanContainer.getBean("projectId");
        if(projectId!=null && projectId!=1){
            Project project = new Project();
            project.setProjectId(projectId);
            Project data = projectService.getProjectDetail(projectId);
//            TableDTO projectList = getProjectList(project);
//            if(projectList.getCount()>1 || projectList.getCount()==0){
//                JOptionPane.showMessageDialog(null,"projectId有问题啊兄弟");
//                dispose();
//            }
//            Vector<Vector<Object>> data = projectList.getData();
            projectNameFIeld.setText(data.getProjectName());
            projectManField.setText(data.getProjectMan());
            startTimeField.setText(simpleDateFormat.format(data.getStartTime()));
            endTimeField.setText(simpleDateFormat.format(data.getEndTime()));
            for (int i=0;i<combobox.size();i++) {
                String option = (String)combobox.get(i);
                if(option.equals(data.getProjectStatus())){
                    projectStatusCombobox.setSelectedIndex(i);
                    break;
                }
            }
            projectIntroductionField.setText(data.getProjectIntroduction());
        }else{
            reloadForm();
        }
    }

    public TableDTO getProjectList(Project project){
        TableDTO tableDTO = projectService.searchList(project);
        return tableDTO;
    }

    @Override
    public void bindAction() {
        saveButton.addActionListener(projectHandler);
        cancelButton.addActionListener(projectHandler);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        dialogPane = new JPanel();
        panel1 = new JPanel();
        saveButton = new JButton();
        cancelButton = new JButton();
        panel2 = new JPanel();
        panel3 = new JPanel();
        projectNameLabel = new JLabel();
        projectNameFIeld = new JTextField();
        projectManLabel = new JLabel();
        projectManField = new JTextField();
        panel4 = new JPanel();
        panel5 = new JPanel();
        startTimeLabel = new JLabel();
        startTimeField = new JTextField();
        endTimeLabel = new JLabel();
        endTimeField = new JTextField();
        panel6 = new JPanel();
        projectStatusLabel = new JLabel();
        projectStatusCombobox = new JComboBox(combobox);
        projectStatusCombobox.setSelectedIndex(-1);
        projectIntroductionLabel = new JLabel();
        projectIntroductionField = new JTextField();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== panel1 ========
            {
                panel1.setLayout(new FlowLayout());

                //---- saveButton ----
                saveButton.setText("保存");
                panel1.add(saveButton);

                //---- cancelButton ----
                cancelButton.setText("取消");
                panel1.add(cancelButton);
            }
            dialogPane.add(panel1, BorderLayout.SOUTH);

            //======== panel2 ========
            {
                panel2.setLayout(new BorderLayout());

                //======== panel3 ========
                {
                    panel3.setLayout(new FlowLayout());

                    //---- projectNameLabel ----
                    projectNameLabel.setText("项目名称");
                    panel3.add(projectNameLabel);
                    panel3.add(projectNameFIeld);

                    //---- projectManLabel ----
                    projectManLabel.setText("项目负责人");
                    panel3.add(projectManLabel);
                    panel3.add(projectManField);
                }
                panel2.add(panel3, BorderLayout.NORTH);

                //======== panel4 ========
                {
                    panel4.setLayout(new BorderLayout());

                    //======== panel5 ========
                    {
                        panel5.setLayout(new FlowLayout());

                        //---- startTimeLabel ----
                        startTimeLabel.setText("开始时间");
                        panel5.add(startTimeLabel);
                        panel5.add(startTimeField);

                        //---- endTimeLabel ----
                        endTimeLabel.setText("结束时间");
                        panel5.add(endTimeLabel);
                        panel5.add(endTimeField);
                    }
                    panel4.add(panel5, BorderLayout.NORTH);

                    //======== panel6 ========
                    {
                        panel6.setLayout(new FlowLayout());

                        //---- projectStatusLabel ----
                        projectStatusLabel.setText("项目状态");
                        panel6.add(projectStatusLabel);
                        panel6.add(projectStatusCombobox);

                        //---- projectIntroductionLabel ----
                        projectIntroductionLabel.setText("项目介绍");
                        panel6.add(projectIntroductionLabel);
                        panel6.add(projectIntroductionField);
                    }
                    panel4.add(panel6, BorderLayout.CENTER);
                }
                panel2.add(panel4, BorderLayout.CENTER);
            }
            dialogPane.add(panel2, BorderLayout.CENTER);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel dialogPane;
    private JPanel panel1;
    private JButton saveButton;
    private JButton cancelButton;
    private JPanel panel2;
    private JPanel panel3;
    private JLabel projectNameLabel;
    private JTextField projectNameFIeld;
    private JLabel projectManLabel;
    private JTextField projectManField;
    private JPanel panel4;
    private JPanel panel5;
    private JLabel startTimeLabel;
    private JTextField startTimeField;
    private JLabel endTimeLabel;
    private JTextField endTimeField;
    private JPanel panel6;
    private JLabel projectStatusLabel;
    private JComboBox projectStatusCombobox;
    private JLabel projectIntroductionLabel;
    private JTextField projectIntroductionField;

    public void reloadForm(){
        this.projectNameFIeld.setText("");
        this.projectManField.setText("");
        this.projectIntroductionField.setText("");
        this.startTimeField.setText("");
        this.endTimeField.setText("");
        this.projectStatusCombobox.setSelectedIndex(-1);
    }

    public void setForm(Project o) {
        this.projectNameFIeld.setText(o.getProjectName()==null?"":o.getProjectName());
        this.projectManField.setText(o.getProjectMan()==null?"":o.getProjectMan());
        this.projectIntroductionField.setText(o.getProjectIntroduction()==null?"":o.getProjectIntroduction());
        this.startTimeField.setText(o.getStartTime()==null?"":simpleDateFormat.format(o.getStartTime()));
        this.endTimeField.setText(o.getProjectName()==null?"":simpleDateFormat.format(o.getEndTime()));
        for (int i = 0; i < combobox.size(); i++) {
            if(o.getProjectStatus().equals(combobox.get(i))){
                projectStatusCombobox.setSelectedIndex(i);
                break;
            }
        }
    }


    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
