/*
 * Created by JFormDesigner on Mon Jul 25 14:03:53 CST 2022
 */

package com.carbon.main;



import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import javax.swing.*;

/**
 * 项目主窗体
 * @author jy
 */
@ViewMain
public class CarbonMain extends DefaultFrame {

    @Autowired
    private ProjectMain projectMain;

    public CarbonMain() {
        initComponents();
        //用户自定义代码
        init();
    }

    //定义桌面
   private JDesktopPane desktopPane;
    private void init() {
        desktopPane=new JDesktopPane(){
            protected void paintComponent(Graphics g)
            {
                //ImageIcon image=new ImageIcon("welcome.jpg");
                Image image=new ImageIcon(CarbonMain.class.getResource("/welcome.jpg")).getImage();
                g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
            }

        };;
        this.getContentPane().add(desktopPane);
        // 窗体最大化;
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        //窗体可见
        this.setVisible(false);
    }



    private void initFrame(JInternalFrame frame){
        desktopPane.removeAll();
        desktopPane.add(frame);
        //最大化
        try {
            frame.setMaximum(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
    }



    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        menuBarCarbon = new JMenuBar();
        menuWMS = new JMenu();
        menuSCM = new JMenu();
        menuEMS = new JMenu();
        menuAudit = new JMenu();

        SaleManagerMenu = new JMenu();
        projectItem = new JMenuItem();

        //======== this ========
        setTitle("\u78b3\u8db3\u8ff9\u7ba1\u7406\u7cfb\u7edf");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon(getClass().getResource("/favicon-small.png")).getImage());
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== menuBarCarbon ========
        {

            //======== menuWMS ========
            {
                menuWMS.setText("\u667a\u80fd\u4ed3\u50a8WMS      ");
                menuWMS.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 20));
            }
            menuBarCarbon.add(menuWMS);

            //======== menuSCM ========
            {
                menuSCM.setText("\u4f9b\u5e94\u94feSCM      ");
                menuSCM.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 20));
            }
            menuBarCarbon.add(menuSCM);

            SaleManagerMenu.setText("销售订单");
            SaleManagerMenu.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 20));
            menuSCM.add(SaleManagerMenu);

            projectItem.setText("销售订单");
            projectItem.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 20));
            projectItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                    projectMain.setVisible(true);
//                    new ProjectMain();
                }
            });
            SaleManagerMenu.add(projectItem);

            //======== menuEMS ========
            {
                menuEMS.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 20));
                menuEMS.setText("\u5236\u9020\u6267\u884cEMS      ");
            }
            menuBarCarbon.add(menuEMS);

            //======== menuAudit ========
            {
                menuAudit.setText("\u5ba1\u6838\u7ba1\u7406");
                menuAudit.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 20));
            }
            menuBarCarbon.add(menuAudit);
        }
        setJMenuBar(menuBarCarbon);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JMenuBar menuBarCarbon;
    private JMenu menuWMS;
    private JMenu menuSCM;
    private JMenu menuEMS;
    private JMenu menuAudit;
    private JMenu SaleManagerMenu;
    private JMenuItem projectItem;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
