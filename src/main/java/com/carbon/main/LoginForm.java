/*
 * Created by JFormDesigner on Thu Jul 28 10:43:32 CST 2022
 */

package com.carbon.main;

import com.carbon.util.DialogUtil;
import com.carbon.util.SecurityUtils;
import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author jy
 */
@ViewMain
public class LoginForm extends DefaultFrame {

    public LoginForm() {
        initComponents();
        //初始化窗体
        init();
    }

    @Autowired
    private CarbonMain carbonMain;

    private void init() {
        setSize(500,400);
        setLocationRelativeTo(null);
        // 使用了SwingX之后就不要一开始就设置为true了，避免闪屏
        setVisible(false);
    }

    /**
     * 登录
     * @param e
     */
    private void btnLogin(ActionEvent e) {
        String userName=txtUserName.getText();
        String password=txtPassword.getText();
        if(userName.isEmpty()){
            DialogUtil.msgDialog("用户名不能为空");
            //这个方法是用来获取焦点的
            txtUserName.requestFocus();
            return;
        }
        if(password.isEmpty()){
            DialogUtil.msgDialog("密码不能为空");
            txtPassword.requestFocus();
            return;
        }
        //登录成功进入
        if(SecurityUtils.isLogin(userName,password)){
            carbonMain.setVisible(true);
            dispose();
        }else{
            DialogUtil.errorDialog("用户名密码错误！");
            txtUserName.requestFocus();
        }
    }

    private void btnExitClick(ActionEvent e) {
        dispose();
        System.exit(0);
    }
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel2 = new JPanel();
        label1 = new JLabel();
        txtUserName = new JTextField();
        panel3 = new JPanel();
        label2 = new JLabel();
        txtPassword = new JPasswordField();
        panel4 = new JPanel();
        btnLogin = new JButton();
        btnExit = new JButton();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("\u767b\u5f55");
        setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/favicon-small.png")).getImage());
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout(0, 30));

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridLayout(3, 1));

                //======== panel2 ========
                {
                    panel2.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 30));

                    //---- label1 ----
                    label1.setText("\u7528\u6237\u540d");
                    label1.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 24));
                    panel2.add(label1);

                    //---- txtUserName ----
                    txtUserName.setColumns(10);
                    txtUserName.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 24));
                    txtUserName.setText("admin");
                    panel2.add(txtUserName);
                }
                contentPanel.add(panel2);

                //======== panel3 ========
                {
                    panel3.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 30));

                    //---- label2 ----
                    label2.setText("\u5bc6   \u7801");
                    label2.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 24));
                    panel3.add(label2);

                    //---- txtPassword ----
                    txtPassword.setColumns(10);
                    txtPassword.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 24));
                    txtPassword.setText("admin123");
                    panel3.add(txtPassword);
                }
                contentPanel.add(panel3);

                //======== panel4 ========
                {
                    panel4.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
                    panel4.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 30));

                    //---- btnLogin ----
                    btnLogin.setText("\u767b\u5f55");
                    btnLogin.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 24));
                    btnLogin.addActionListener(e -> btnLogin(e));
                    panel4.add(btnLogin);

                    //---- btnExit ----
                    btnExit.setText("\u9000\u51fa");
                    btnExit.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 24));
                    btnExit.addActionListener(e -> btnExitClick(e));
                    panel4.add(btnExit);
                }
                contentPanel.add(panel4);
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel panel2;
    private JLabel label1;
    private JTextField txtUserName;
    private JPanel panel3;
    private JLabel label2;
    private JPasswordField txtPassword;
    private JPanel panel4;
    private JButton btnLogin;
    private JButton btnExit;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
