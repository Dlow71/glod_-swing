/*
 * Created by JFormDesigner on Mon Jun 05 10:55:40 CST 2023
 */

package com.carbon.main;

import com.carbon.common.domain.Project;
import com.carbon.common.handler.ProjectHandler;
import com.carbon.common.res.TableDTO;
import com.carbon.common.service.project.ProjectService;
import com.carbon.common.table.project.ProjectTable;
import com.carbon.common.table.project.ProjectTableModel;
import com.carbon.util.SwingX.SizeUtil;
import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author 山沐与山
 */
@ViewMain
public class ProjectMain extends DefaultFrame {
    private static Vector<String> projectStatus = new Vector<>();
    @Autowired
    private ProjectHandler projectHandler;
    @Autowired
    private ProjectService projectService;

    @Autowired
    private CarbonMain carbonMain;

    {
        projectStatus.addElement("正常");
        projectStatus.addElement("异常");
        projectStatus.addElement("挂起");
    }

    public static Vector<String> getProjectStatus(){
        return projectStatus;
    }

    public ProjectMain() {
        super();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                dispose();
                // 弹出新窗口不再需要new对象来弹，直接setVisible就行了
                carbonMain.setVisible(true);
            }
        });
        initComponents();
        SizeUtil.setSize(this.getClass(),this);
//        bindAction();
//        loadData();
        setExtendedState(MAXIMIZED_BOTH);
        // 使用了SwingX之后就不要一开始就设置为true了，避免闪屏

    }

    public ArrayList<Long> getSelectIds(){
        int[] selectedRows = table1.getSelectedRows();
        ArrayList<Long> ids = new ArrayList<>();
        for (int selectedRow : selectedRows) {
            Long id = (Long) table1.getValueAt(selectedRow, 0);
            if(id==null){
                ProjectTableModel.deleteRow(selectedRow);
                continue;
            }
            ids.add(id);
        }
        return ids;
    }

    // 每次窗口打开都会自动调用这个方法获取数据
    @Override
    public void loadData() {
        TableDTO tableDTO = getTableDto();
        ProjectTableModel projectTableModel = ProjectTableModel.assembleTable(tableDTO.getData());
        this.table1.setModel(projectTableModel);
    }

    private TableDTO getTableDto() {
        Project project = new Project();
        project.setProjectName(this.projectNameField.getText());
        if(this.projectStatusCombobox.getSelectedItem()!=null)
        project.setProjectStatus(this.projectStatusCombobox.getSelectedItem()+"");
        TableDTO tableDTO = projectService.searchList(project);
        return tableDTO;
    }

    // 只有对象初始化的时候才会调用绑定事件，因此只调用一次
    @Override
    public void bindAction() {
        addBtn.addActionListener(projectHandler);
        updateBtn.addActionListener(projectHandler);
        delBtn.addActionListener(projectHandler);
        searchBtn.addActionListener(projectHandler);
        resetBtn.addActionListener(projectHandler);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        dialogPane = new JPanel();
        panel1 = new JPanel();
        projectNameLabel = new JLabel();
        projectNameField = new JTextField();
        projectStatusLabel = new JLabel();
        projectStatusCombobox = new JComboBox(projectStatus);
        projectStatusCombobox.setSelectedIndex(-1);
        searchBtn = new JButton();
        resetBtn = new JButton();
        scrollPane1 = new JScrollPane();
        table1 = new ProjectTable();
        panel2 = new JPanel();
        addBtn = new JButton();
        updateBtn = new JButton();
        delBtn = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== panel1 ========
            {
                panel1.setLayout(new FlowLayout());

                //---- projectNameLabel ----
                projectNameLabel.setText("项目名称");
                panel1.add(projectNameLabel);
                panel1.add(projectNameField);

                //---- projectStatusLabel ----
                projectStatusLabel.setText("项目状态");
                panel1.add(projectStatusLabel);
                panel1.add(projectStatusCombobox);

                //---- searchBtn ----
                searchBtn.setText("搜索");
                panel1.add(searchBtn);

                //---- resetBtn ----
                resetBtn.setText("重置");
                panel1.add(resetBtn);
            }
            dialogPane.add(panel1, BorderLayout.NORTH);

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(table1);
            }
            dialogPane.add(scrollPane1, BorderLayout.CENTER);

            //======== panel2 ========
            {
                panel2.setLayout(new FlowLayout());

                //---- addBtn ----
                addBtn.setText("新增");
                panel2.add(addBtn);

                //---- updateBtn ----
                updateBtn.setText("修改");
                panel2.add(updateBtn);

                //---- delBtn ----
                delBtn.setText("删除");
                panel2.add(delBtn);
            }
            dialogPane.add(panel2, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel dialogPane;
    private JPanel panel1;
    private JLabel projectNameLabel;
    private JTextField projectNameField;
    private JLabel projectStatusLabel;
    private JComboBox projectStatusCombobox;
    private JButton searchBtn;
    private JButton resetBtn;
    private JScrollPane scrollPane1;
    private ProjectTable table1;
    private JPanel panel2;
    private JButton addBtn;
    private JButton updateBtn;
    private JButton delBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on

    public static void setProjectStatus(Vector<String> projectStatus) {
        ProjectMain.projectStatus = projectStatus;
    }

    public JPanel getDialogPane() {
        return dialogPane;
    }

    public void setDialogPane(JPanel dialogPane) {
        this.dialogPane = dialogPane;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JLabel getProjectNameLabel() {
        return projectNameLabel;
    }

    public void setProjectNameLabel(JLabel projectNameLabel) {
        this.projectNameLabel = projectNameLabel;
    }

    public JTextField getProjectNameField() {
        return projectNameField;
    }

    public void setProjectNameField(JTextField projectNameField) {
        this.projectNameField = projectNameField;
    }

    public JLabel getProjectStatusLabel() {
        return projectStatusLabel;
    }

    public void setProjectStatusLabel(JLabel projectStatusLabel) {
        this.projectStatusLabel = projectStatusLabel;
    }

    public JComboBox getProjectStatusCombobox() {
        return projectStatusCombobox;
    }

    public void setProjectStatusCombobox(JComboBox projectStatusCombobox) {
        this.projectStatusCombobox = projectStatusCombobox;
    }

    public JButton getSearchBtn() {
        return searchBtn;
    }

    public void setSearchBtn(JButton searchBtn) {
        this.searchBtn = searchBtn;
    }

    public JButton getResetBtn() {
        return resetBtn;
    }

    public void setResetBtn(JButton resetBtn) {
        this.resetBtn = resetBtn;
    }

    public JScrollPane getScrollPane1() {
        return scrollPane1;
    }

    public void setScrollPane1(JScrollPane scrollPane1) {
        this.scrollPane1 = scrollPane1;
    }

    public JTable getTable1() {
        return table1;
    }

    public void setTable1(ProjectTable table1) {
        this.table1 = table1;
    }

    public JPanel getPanel2() {
        return panel2;
    }

    public void setPanel2(JPanel panel2) {
        this.panel2 = panel2;
    }

    public JButton getAddBtn() {
        return addBtn;
    }

    public void setAddBtn(JButton addBtn) {
        this.addBtn = addBtn;
    }

    public JButton getUpdateBtn() {
        return updateBtn;
    }

    public void setUpdateBtn(JButton updateBtn) {
        this.updateBtn = updateBtn;
    }

    public JButton getDelBtn() {
        return delBtn;
    }

    public void setDelBtn(JButton delBtn) {
        this.delBtn = delBtn;
    }

    public Project getProjectForm(Integer selectRow) {
        Project project = new Project();
        project.setProjectName((String) table1.getValueAt(selectRow,1));
        project.setProjectMan((String) table1.getValueAt(selectRow,2));
        project.setStartTime((Date) table1.getValueAt(selectRow,3));
        project.setEndTime((Date) table1.getValueAt(selectRow,4));
        project.setProjectStatus((String) table1.getValueAt(selectRow,5));
        project = projectService.getProjectDetail((Long)table1.getValueAt(selectRow,0));
        return project;
    }

    public int getSelectRow() {
        return table1.getSelectedRow();
    }
}
