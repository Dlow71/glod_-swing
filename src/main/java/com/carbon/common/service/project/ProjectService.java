package com.carbon.common.service.project;

import com.carbon.common.domain.Project;
import com.carbon.common.res.TableDTO;

public interface ProjectService {

    TableDTO searchList(Project project);

    int addProject(Project project);

    int updateProject(Project project);

    Project getProjectDetail(Long projectId);

    boolean delById(Long aLong);
}
