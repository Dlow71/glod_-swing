package com.carbon.common.service.project.impl;

import com.carbon.common.domain.Project;
import com.carbon.common.res.TableDTO;
import com.carbon.common.service.project.ProjectService;
import com.carbon.util.DBUtil;
import com.carbon.util.DialogUtil;
import com.carbon.util.StringUtils;
import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.annotation.Service;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Vector;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Override
    public TableDTO searchList(Project project) {
        StringBuilder sql = new StringBuilder("select * from test_project where 1=1 ");
        if(project.getProjectName()!=null && StringUtils.isNotBlank(project.getProjectName())){
            sql.append("and project_name like '%"+project.getProjectName().trim()+"%'");
        }
        if(project.getProjectStatus()!=null && StringUtils.isNotBlank(project.getProjectStatus())){
            sql.append("and project_status=? ");
        }
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement(sql.toString());
            if(project.getProjectStatus()!=null && StringUtils.isNotBlank(project.getProjectStatus()))
            ps.setString(1,project.getProjectStatus());
            rs = ps.executeQuery();
            Vector<Vector<Object>> vector = new Vector<>();
            while(rs.next()){
                Vector<Object> data = new Vector<>();
                Long project_id = rs.getLong("project_id");
                String project_name = rs.getString("project_name");
                String project_man = rs.getString("project_man");
                Date start_time = rs.getDate("start_time");
                Date end_time = rs.getDate("end_time");
                String project_status = rs.getString("project_status");
                String project_introduction = rs.getString("project_introduction");
                data.addElement(project_id);
                data.addElement(project_name);
                data.addElement(project_man);
                data.addElement(start_time);
                data.addElement(end_time);
                data.addElement(project_status );
                data.addElement(project_introduction);
                vector.addElement(data);
            }
            return new TableDTO(vector, vector.size());
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            DBUtil.closeRs(rs);
            DBUtil.closePs(ps);
            DBUtil.closeSourceConnection(conn);
        }
        return null;
    }

    @Override
    public int addProject(Project project) {
        StringBuilder insertSql = new StringBuilder("insert into test_project (project_name," +
                "project_man,start_time,end_time,project_status,project_introduction)" +
                " values(?,?,?,?,?,?)");
        SimpleDateFormat simpleDateFormat = DefaultFrame.simpleDateFormat;
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = DBUtil.getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(insertSql.toString());
            ps.setString(1,project.getProjectName());
            ps.setString(2,project.getProjectMan());
            ps.setString(3,simpleDateFormat.format(project.getStartTime()));
            ps.setString(4,simpleDateFormat.format(project.getEndTime()));
            ps.setString(5,project.getProjectStatus());
            ps.setString(6,project.getProjectIntroduction());
            int row = ps.executeUpdate();
            conn.commit();
            return row;
        } catch (SQLException e) {
            e.printStackTrace();
            DialogUtil.errorDialog("保存失败");
        } finally {
            DBUtil.closePs(ps);
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            DBUtil.closeSourceConnection(conn);
        }
        return 0;
    }

    @Override
    public int updateProject(Project project) {
        StringBuilder updateSql = new StringBuilder("update test_project set project_name=?,project_man=?," +
                "start_time=?,end_time=?,project_status=?,project_introduction=? where project_id=");
        updateSql.append(project.getProjectId());
        SimpleDateFormat simpleDateFormat = DefaultFrame.simpleDateFormat;
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = DBUtil.getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(updateSql.toString());
            ps.setString(1,project.getProjectName());
            ps.setString(2,project.getProjectMan());
            ps.setString(3,simpleDateFormat.format(project.getStartTime()));
            ps.setString(4,simpleDateFormat.format(project.getEndTime()));
            ps.setString(5,project.getProjectStatus());
            ps.setString(6,project.getProjectIntroduction());
            int row = ps.executeUpdate();
            conn.commit();
            return row;
        } catch (Exception e) {
            DialogUtil.errorDialog("保存失败");
            try {
                conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            DBUtil.closePs(ps);
            DBUtil.closeSourceConnection(conn);
        }
        return 0;
    }

    @Override
    public Project getProjectDetail(Long id) {
        StringBuilder sql = new StringBuilder("select * from test_project where project_id="+id);
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            Project project = new Project();
            while(rs.next()){
                Long project_id = rs.getLong("project_id");
                String project_name = rs.getString("project_name");
                String project_man = rs.getString("project_man");
                Date start_time = rs.getDate("start_time");
                Date end_time = rs.getDate("end_time");
                String project_status = rs.getString("project_status");
                String project_introduction = rs.getString("project_introduction");
                project.setProjectId(project_id);
                project.setProjectName(project_name);
                project.setProjectMan(project_man);
                project.setStartTime(start_time);
                project.setEndTime(end_time);
                project.setProjectStatus(project_status);
                project.setProjectIntroduction(project_introduction);
            }
            return project;
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            DBUtil.closeRs(rs);
            DBUtil.closePs(ps);
            DBUtil.closeSourceConnection(conn);
        }
        return null;
    }

    @Override
    public boolean delById(Long id) {
        StringBuilder sql = new StringBuilder("delete from test_project where project_id=" + id);
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = DBUtil.getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(sql.toString());
            int row = ps.executeUpdate();
            conn.commit();
            return row==1;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            DBUtil.closePs(ps);
            DBUtil.closeSourceConnection(conn);
        }
        return false;
    }

}
