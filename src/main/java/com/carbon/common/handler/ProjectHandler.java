package com.carbon.common.handler;


import com.carbon.common.domain.Project;
import com.carbon.common.service.project.ProjectService;
import com.carbon.main.EditProjectMain;
import com.carbon.main.ProjectMain;
import com.carbon.util.DialogUtil;
import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.ioc.BeanContainer;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import com.carbon.util.SwingX.ioc.annotation.Handler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Handler
public class ProjectHandler implements ActionListener {
    private BeanContainer beanContainer = BeanContainer.getInstance();

    @Autowired
    private ProjectMain projectMain;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private EditProjectMain editProjectMain;

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();
        String text = button.getText();
        JTextField projectNameField = projectMain.getProjectNameField();
        JComboBox projectStatusCombobox = projectMain.getProjectStatusCombobox();

        ArrayList<Long> ids = projectMain.getSelectIds();
        // switch这里其实可以给每个case里面都加{}，性能更高，而且不会出现上面的变量影响下面变量命名的问题
        switch (text) {
            case "搜索":
                projectMain.loadData();
            break;
            case "重置":
                projectNameField.setText("");
                projectStatusCombobox.setSelectedIndex(-1);
            break;
            case "新增":
                beanContainer.removeBean("projectId");
                // 别看这里只调用了setVisible，其实这个是调用的重写方法
                // 重写方法里面调用了loadData
                // 之所以这样做，也是为了解耦，专注业务就行了
                editProjectMain.setVisible(true);
            break;
            case "修改":
                int selectRow = projectMain.getSelectRow();
                if(selectRow==-1){
                    DialogUtil.msgDialog("修改请选择一行数据进行修改");
                }
                beanContainer.addBean("projectId",projectMain.getTable1().getValueAt(selectRow,0));
//                Project p = projectMain.getProjectForm(selectRow);
//                editProjectMain.setForm(p);
                editProjectMain.setVisible(true);
            break;
            case "删除":
                if(ids.size()<1){
                    DialogUtil.msgDialog("请至少选择一行");
                    return;
                }
                int select = JOptionPane.showConfirmDialog(null, "是否删除Id为" + ids.get(0) + "的项吗", "请选择", JOptionPane.YES_NO_OPTION);
                if(select==1) return;
                // 这里其实不如写个delByIds，可以传个Id集合过去，
                // 这里省时间就暂时只根据单个id去删，主要是整个表格也就限定了只能单选一行
                boolean flag = projectService.delById(ids.get(0));
                if(flag){
                    DialogUtil.msgDialog("删除成功");
                    projectMain.loadData();
                }else{
                    DialogUtil.errorDialog("删除失败");
                }
            break;
            case "保存":
                try {
                    Project project = new Project();
                    SimpleDateFormat format = DefaultFrame.simpleDateFormat;
                    Long id = (Long)beanContainer.getBean("projectId");
                    String projectName = editProjectMain.getProjectNameFIeld().getText();
                    String projectMan = editProjectMain.getProjectManField().getText();
                    Date startTime = format.parse(editProjectMain.getStartTimeField().getText());
                    Date endTime = format.parse(editProjectMain.getEndTimeField().getText());
                    String projectStatus = (String) editProjectMain.getProjectStatusCombobox().getSelectedItem();
                    String projectIntroduction = editProjectMain.getProjectIntroductionField().getText();

                    project.setProjectId(id);
                    project.setProjectName(projectName);
                    project.setProjectMan(projectMan);
                    project.setStartTime(startTime);
                    project.setEndTime(endTime);
                    project.setProjectStatus(projectStatus);
                    project.setProjectIntroduction(projectIntroduction);

                    int row = 0;
                    if(project.getProjectId()!=null){
                        row = projectService.updateProject(project);
                    }else{
                        row = projectService.addProject(project);
                    }
                    if(row!=1){
                        JOptionPane.showMessageDialog(null,"保存失败");
                    }else{
                        JOptionPane.showMessageDialog(null,"保存成功");
                        beanContainer.removeBean("projectId");
                        projectMain.loadData();
                    }
                    beanContainer.removeBean("projectId");
                    editProjectMain.dispose();
                } catch (ParseException ex) {
                    DialogUtil.errorDialog("请填写正确的时间格式 yyyy-MM-dd");
                    ex.printStackTrace();
                }

            break;
            case "取消":
                editProjectMain.dispose();
            break;
        }
    }
}
