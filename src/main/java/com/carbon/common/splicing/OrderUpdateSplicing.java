package com.carbon.common.splicing;

public class OrderUpdateSplicing {
    private Long orderId;

    public OrderUpdateSplicing() {
    }

    public OrderUpdateSplicing(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
