package com.carbon.common.res;

import java.util.Vector;

public class TableDTO {
    private Vector<Vector<Object>> data;
    private Integer count;

    public TableDTO(Vector<Vector<Object>> data, Integer count) {
        this.data = data;
        this.count = count;
    }

    public Vector<Vector<Object>> getData() {
        return data;
    }

    public void setData(Vector<Vector<Object>> data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
