package com.carbon.common.table.project;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class ProjectTableModel extends DefaultTableModel {
    private static Vector<String> columns = new Vector<>();
    static{
        columns.addElement("编号");
        columns.addElement("项目名称");
        columns.addElement("负责人");
        columns.addElement("开始日期");
        columns.addElement("结束日期");
        columns.addElement("项目状态");
    }

    public Vector<String> getColumns(){
        return columns;
    }

    private static ProjectTableModel projectTableModel = new ProjectTableModel();

    public ProjectTableModel() {
        super(null,columns);
    }

    public static ProjectTableModel assembleTable(Vector<Vector<Object>> data){
         projectTableModel.setDataVector(data,columns);
         return projectTableModel;
    }

    public static void updateModel(Vector<Vector<Object>> data){
        projectTableModel.setDataVector(data,columns);
    }

    public static void deleteRow(int row){
        if(row<1) return;
        projectTableModel.removeRow(row);
    }

    public static void addRow(){
        projectTableModel.addRow(new Vector());
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
