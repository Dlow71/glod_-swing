package com.carbon.common.table.project;

import com.carbon.util.SwingX.SizeUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class ProjectTable extends JTable {
    public ProjectTable(){
        setRowHeight(30);
        setFont(SizeUtil.font);
        DefaultTableCellRenderer dc = new DefaultTableCellRenderer();
        dc.setHorizontalAlignment(JLabel.CENTER);
        setDefaultRenderer(Object.class,dc);
        // 设置重新排序允许
        getTableHeader().setReorderingAllowed(false);
        getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
}
