package com.carbon.common.dao;

import com.carbon.common.Base;
import com.carbon.common.domain.SysSerialConfig;
import com.carbon.exception.CarbonException;
import com.carbon.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SysSerialConfigDao extends Base {
    /**
     * 查询流水号配置
     *
     * @param id 流水号配置ID
     * @return 流水号配置
     */
    public SysSerialConfig selectSysSerialConfigById(Long id){
        SysSerialConfig s=null;
        String sql="select id, biz_type, prefix, max_no from sys_serial_config where id=?";
        //数据库连接
        try(Connection conn= DBUtil.getConnection()){
            PreparedStatement pstmt=conn.prepareStatement(sql);
            pstmt.setLong(1,id);
            ResultSet rs=pstmt.executeQuery();
            while (rs.next()){
                 s=new SysSerialConfig();
                s.setId(rs.getLong("id"));
                s.setMaxNo(rs.getLong("max_no"));
                s.setBizType(rs.getString("biz_type"));
                s.setPrefix(rs.getString("prefix"));
            }

        }catch (Exception e){
            e.printStackTrace();
            throw  new CarbonException(e.getMessage());
        }
        return s;
    }

    public List<SysSerialConfig> selectSysSerialConfigList(SysSerialConfig sysSerialConfig){
        List<SysSerialConfig> list=new ArrayList<>();
        String sql="select id, biz_type, prefix, max_no from sys_serial_config where 1=1";
        //where条件语句

        StringBuilder buf = new StringBuilder();
        if (sysSerialConfig.getBizType()!= null && !"".equals(sysSerialConfig.getBizType())) {
            buf.append(" and biz_type ='");
            buf.append(sysSerialConfig.getBizType());
            buf.append("'");
        }
        if (sysSerialConfig.getMaxNo()!= null && !"".equals(sysSerialConfig.getMaxNo())) {
            buf.append(" and max_no =");
            buf.append(sysSerialConfig.getMaxNo());
        }
        if (sysSerialConfig.getPrefix()!= null && !"".equals(sysSerialConfig.getPrefix())) {
            buf.append(" and prefix ='");
            buf.append(sysSerialConfig.getPrefix());
            buf.append("'");
        }
        sql = sql + buf.toString();

        log.debug(sql);
        //数据库连接
        try(Connection conn= DBUtil.getConnection()){
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            while (rs.next()){
                SysSerialConfig s=new SysSerialConfig();
                s.setId(rs.getLong("id"));
                s.setMaxNo(rs.getLong("max_no"));
                s.setBizType(rs.getString("biz_type"));
                s.setPrefix(rs.getString("prefix"));
                list.add(s);
            }

        }catch (Exception e){
            e.printStackTrace();
            throw  new CarbonException(e.getMessage());
        }
        return  list;
    }

    /**
     * 修改流水号配置
     *
     * @param sysSerialConfig
     *            流水号配置
     * @return 结果
     */
    public int updateSysSerialConfig(SysSerialConfig sysSerialConfig) {
        int count=0;
        String sql="update sys_serial_config set biz_type=?,prefix=?,max_no=? where id=?";
        try(Connection conn= DBUtil.getConnection()){
            PreparedStatement pstmt=conn.prepareStatement(sql);
            pstmt.setString(1,sysSerialConfig.getBizType());
            pstmt.setString(2,sysSerialConfig.getPrefix());
            pstmt.setLong(3,sysSerialConfig.getMaxNo());
            pstmt.setLong(4,sysSerialConfig.getId());
            count=pstmt.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
            throw  new CarbonException(e.getMessage());
        }
        return count;
    }

    /**
     * 测试
     * @param args
     */
    public static void main(String[] args) {
        SysSerialConfigDao dao=new SysSerialConfigDao();
        SysSerialConfig s=dao.selectSysSerialConfigById(1L);
        System.out.println(s);
        s.setPrefix("");
        s.setBizType("");
        s.setMaxNo(2L);
        List<SysSerialConfig> list=dao.selectSysSerialConfigList(s);
        System.out.println(list);

    }

}
