package com.carbon.common.req;


//TODO 这里的属性还没做适配，自动生成SQL没法搞
public class BaseRequest {
    private int pageNow;
    private int pageSize;
    private Integer start;
    private String searchKey;

    public Integer getStart(){
        return (pageNow-1)*pageSize;
    }

    public int getPageNow() {
        return pageNow;
    }

    public void setPageNow(int pageNow) {
        this.pageNow = pageNow;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }
}
