package com.carbon.common.enums;

/**
 * 操作状态
 * 
 * @author ljxl
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
