package com.carbon.common;

public interface CallBack {
    public void doEvent(Object object);
}
