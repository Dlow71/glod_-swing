package com.carbon.common.aspect;

import com.carbon.util.SwingX.aop.annotation.AspectPlus;
import com.carbon.util.SwingX.aop.annotation.Order;
import com.carbon.util.SwingX.aop.aspect.DefaultAspect;


import java.lang.reflect.Method;

//@Aspect(value = Service.class)
@AspectPlus(pointcut = "within(com.carbon.common.service.project.impl.*)")
@Order(2)
public class SecondAspect extends DefaultAspect {
    @Override
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable {
        System.out.println("第二个Aspect的Before已执行");
    }

    @Override
    public Object afterReturning(Class<?> targetClass, Method method, Object[] args, Object returnValue) throws Throwable {
        System.out.println("第二个Aspect的After已执行");
        return returnValue;
    }
}
