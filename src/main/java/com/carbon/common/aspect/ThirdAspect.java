package com.carbon.common.aspect;

import com.carbon.util.SwingX.aop.annotation.Aspect;
import com.carbon.util.SwingX.aop.annotation.Order;
import com.carbon.util.SwingX.aop.aspect.DefaultAspect;
import com.carbon.util.SwingX.ioc.annotation.Service;

import java.lang.reflect.Method;

@Aspect(Service.class)
@Order(value = 3)
public class ThirdAspect extends DefaultAspect {
    @Override
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable {
        System.out.println("第三个aspect的before执行了");
    }

    @Override
    public Object afterReturning(Class<?> targetClass, Method method, Object[] args, Object returnValue) throws Throwable {
        System.out.println("第三个aspect的after执行了");
        return returnValue;
    }
}
