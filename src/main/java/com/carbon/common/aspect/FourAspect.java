package com.carbon.common.aspect;

import com.carbon.util.SwingX.aop.annotation.AspectPlus;
import com.carbon.util.SwingX.aop.annotation.Order;
import com.carbon.util.SwingX.aop.aspect.DefaultAspect;

import java.lang.reflect.Method;

@AspectPlus(pointcut = "within(com.carbon.common.service.project.impl.*)")
@Order(4)
public class FourAspect extends DefaultAspect {
    @Override
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable {
        System.out.println("第四个Aspect的before执行了");
    }

    @Override
    public Object afterReturning(Class<?> targetClass, Method method, Object[] args, Object returnValue) throws Throwable {
        System.out.println("第四个Aspect的after执行了");
        return returnValue;
    }
}
