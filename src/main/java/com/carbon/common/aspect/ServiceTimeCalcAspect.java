package com.carbon.common.aspect;

import com.carbon.util.SwingX.aop.annotation.Aspect;
import com.carbon.util.SwingX.aop.annotation.Order;
import com.carbon.util.SwingX.aop.aspect.DefaultAspect;
import com.carbon.util.SwingX.ioc.annotation.Service;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

@Aspect(value = Service.class)
//@AspectPlus(pointcut = "within(com.carbon.common.service.project.impl.*)")
@Order(0)
@Slf4j
public class ServiceTimeCalcAspect extends DefaultAspect {
    private long timestampCache;

    @Override
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable {
        log.info("开始计时，执行的类是[{}]，执行的方法是[{}]，参数是[{}]",targetClass.getName()
                ,method.getName(),args);
        timestampCache = System.currentTimeMillis();
    }

    @Override
    public Object afterReturning(Class<?> targetClass, Method method, Object[] args, Object returnValue) throws Throwable {
        long end = System.currentTimeMillis();
        long costTime = end - timestampCache;
        log.info("结束计时，执行的类是[{}]，执行的方法是[{}]，参数是[{}]，时间为[{}]",targetClass.getName()
                ,method.getName(),args,costTime);
        return returnValue;
    }

}
