package com.carbon.common.domain;



/**
 * 流水号配置对象 sys_serial_config
 * 
 * @author ljxl
 * @date 2022-06-23
 */

public class SysSerialConfig{
    private static final long serialVersionUID = 1L;

    /** 编号 */

    private Long id;

    /** 业务类型 */

    private String bizType;

    /** 前缀 */

    private String prefix;

    /** 最大号 */

    private Long maxNo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBizType(String bizType) 
    {
        this.bizType = bizType;
    }

    public String getBizType() 
    {
        return bizType;
    }
    public void setPrefix(String prefix) 
    {
        this.prefix = prefix;
    }

    public String getPrefix() 
    {
        return prefix;
    }
    public void setMaxNo(Long maxNo) 
    {
        this.maxNo = maxNo;
    }

    public Long getMaxNo() 
    {
        return maxNo;
    }

    @Override
    public String toString() {
        return "SysSerialConfig{" +
                "id=" + id +
                ", bizType='" + bizType + '\'' +
                ", prefix='" + prefix + '\'' +
                ", maxNo=" + maxNo +
                '}';
    }
}
