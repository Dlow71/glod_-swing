/*
 * Created by JFormDesigner on Thu Jul 28 11:25:37 CST 2022
 */

package com.carbon;

import com.carbon.main.LoginForm;
import com.carbon.util.SystemDict;
import com.carbon.util.SwingX.SwingAdapt.DefaultFrame;
import com.carbon.util.SwingX.SwingX;
import com.carbon.util.SwingX.ioc.DependencyInjector;
import com.carbon.util.SwingX.ioc.annotation.Autowired;
import com.carbon.util.SwingX.ioc.annotation.ViewMain;
import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

/**
 * @author jy
 */
@ViewMain(isCurtain = true)
public class SplashWindow extends DefaultFrame implements ActionListener {

    @Autowired
    private LoginForm loginForm;

    public SplashWindow() {
        super();
        initComponents();
        init();
    }

    protected final Logger log=Logger.getLogger(SplashWindow.class);
    private Timer timer;
    private void init() {
        // 50
        timer=new Timer(10,this);
        timer.start();
        this.setSize(800,500);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        setAlwaysOnTop(true);

        // 提前加载
        log.debug(SystemDict.userMap);
    }

    //这里是监听进度加载条的
    @Override
    public void actionPerformed(ActionEvent e) {
        int value=pgBar.getValue();
        //加载条自动递增
        if(value<100){
            pgBar.setValue(++value);
        }else{
            //到一百后直接加载loginForm
            timer.stop();
            if(!DependencyInjector.getFinishFlag()){
                timer = new Timer(10,this);
                return;
            }
            loginForm.setVisible(true);

            //窗体销毁
            dispose();
        }
    }

    public static void main(String[] args) {
        SwingX.initialize("com.carbon");
    }

    private void pgBarStateChanged(ChangeEvent e) {

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        labWelcome = new JLabel();
        pgBar = new JProgressBar();
        label1 = new JLabel();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //---- labWelcome ----
        labWelcome.setIcon(new ImageIcon(getClass().getResource("/welcome.jpg")));
        contentPane.add(labWelcome, BorderLayout.EAST);

        //---- pgBar ----
        pgBar.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 12));
        pgBar.setBackground(new Color(0xcccccc));
        pgBar.addChangeListener(e -> pgBarStateChanged(e));
//        contentPane.add(pgBar, BorderLayout.SOUTH);

        //---- label1 ----
        label1.setText("text");
        contentPane.add(label1, BorderLayout.CENTER);
        setSize(535, 375);
        setLocationRelativeTo(null);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel labWelcome;
    private JProgressBar pgBar;
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
